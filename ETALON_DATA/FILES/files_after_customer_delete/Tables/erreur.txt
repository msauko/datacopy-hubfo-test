# Date d'exportation : Mon Dec  4 07:59:11 1995
#
# En cours ETEBAC 5 Page A-85
error = BSC_RIC, 1
	title = "Probleme physique sur coupleur"

error = BSC_RIC, 2
	title = "Commande incompatible avec le protocole BSC"

error = BSC_RIC, 5
	title = "Fin Anormale (r��mission)"

error = BSC_RIC, 7
	title = "Serveur BSC absent"

error = BSC_RIC, 9
	title = "Commande asynchrone en cours de traitement"

error = BSC_RIC, 240
	title = "R�ception d'un bloc multiple (ITB)"

error = BSC_RIC, 241
	title = "R�ception du bloc EOT"

error = BSC_RIC, 242
	title = "R�ception du bloc ETX"

error = BSC_RIC, 243
	title = "R�ception du bloc ETB"

error = BSC_RIC, 244
	title = "R�ception du bloc DLE/EOT (d�connexion)"

error = BSC_RIC, 245
	title = "Collision ENQ (collision d'appels)"

error = BSC_RIC, 246
	title = "Collision RVI"

error = ETEBAC2, 2108
	title = "Fichier non disponible"
	action = "Pr�parer un fichier"

error = ETEBAC3, 2100
	title = "Longueur de carte param�tre invalide"
	action = "Forcer la longueur a 80 sur le poste client"

error = ETEBAC3, 2102
	title = "Sens du transfert absent ou inconnu"
	action = "Le sens doit �tre \"A\" ou \"R\" dans la carte parametre"

error = ETEBAC3, 2104
	title = "Identifiant client incorrect ou absent"
	action = "Remplir le champ Identit� Contract dans la carte parametre"

error = ETEBAC3, 2106
	title = "Mot de passe incorrect ou absent"
	action = "Verifier le contrat ETEBAC3 dans secadm"

error = ETEBAC3, 2108
	title = "Nature de fichier ou d'application incorrecte ou absente"
	action = "Pr�parer un fichier"

error = ETEBAC3, 2110
	title = "Longueur d'enregistrement incorrecte ou absente"

error = ETEBAC3, 2112
	title = "Date de fichier incorrecte ou absente"

error = ETEBAC3, 2114
	title = "Num�ro de version ou de s�quence incorrecte"

error = ETEBAC3, 2116
	title = "Type de fichier incomptatible avec sens de transfert"

error = ETEBAC3, 2118
	title = "Carte-Param�tre non reconnue"
	action = "Modifier la carte sur le poste client"

error = ETEBAC3, 2120
	title = "Code banque invalide"

error = ETEBAC3, 2122
	title = "Autre anomalie sur carte-param�tre"

error = ETEBAC3, 2199
	title = "Anomalie sur autre param�tre"

#### 2200 #####
error = ETEBAC3, 2200
	title = "Client inconnu"

error = ETEBAC3, 2202
	title = "Mot de passe invalide"

error = ETEBAC3, 2204
	title = "Refus de taxation au demand�"

error = ETEBAC3, 2206
	title = "Client non habilit� pour cette nature de fichier"
	action = "V�rifier les services contract�s pour ce contrat ETEBAC"

error = ETEBAC3, 2208
	title = "Num�ro appelant invalide"

error = ETEBAC3, 2212
	title = "Acces interdit en reception"

error = ETEBAC3, 2214
	title = "Acces refus� suite a tentative avec mot de passe invalide"

error = ETEBAC3, 2245
	title = "Contrat ETEBAC Absent"
	action = "V�rifier la base Contrat ETEBAC3"

error = ETEBAC3, 2246
	title = "Client ETEBAC Absent"
	action = "V�rifier la base Client"

#### 2300 #####
error = ETEBAC3, 2300
	title = "Application (service) ferm�e"

error = ETEBAC3, 2302
	title = "Acc�s en �mission provisoirement ferm�e"

error = ETEBAC3, 2304
	title = "Acc�s en reception provisoirement ferm�e"

error = ETEBAC3, 2306
	title = "Application non encore disponible"

error = ETEBAC3, 2308
	title = "Le service n'est plus assur�"

error = ETEBAC3, 2310
	title = "Le service non encore assur�"

#### 2400 #####
error = ETEBAC3, 2400
	title = "Fichier non disponible"

error = ETEBAC3, 2402
	title = "Fichier d�ja demand�"

error = ETEBAC3, 2404
	title = "Pas de fichier ce jour pour cette application"

error = ETEBAC3, 2406
	title = "Date demand�e trop ancienne"

error = ETEBAC3, 2420
	title = "Fichier d�j� re�u"

#### 3100 #####
error = ETEBAC3, 3100
	title = "Heure d�pass�e : Traitement lors de la prochaine vacation"

error = ETEBAC3, 3102
	title = "No de compostage/transfert"

error = ETEBAC3, 3104
	title = "Un fichier est disponible pour cette application"

error = ETEBAC3, 3106
	title = "Un fichier est disponible pour d'autres applications"

error = ETEBAC3, 3108
	title = "Tarification Heures Creuses"

error = ETEBAC3, 3110
	title = "Tarification Heures Pleines"

#### ETEBAC5 #####
error = ETEBAC5, 200		# AVOIR doublon dans la doc (erreur 200) ?
	# DTF : F.RESTART F.ABORT
	title = "Erreur de transmission"
	action = "Verifier anomalie CRC sur PAD"

error = ETEBAC5, 200
	# CREATE : F.CREATE
	title = "Caracteristiques du fichier insuffisante"

error = ETEBAC5, 201
	# CREATE : F.CREATE
	title = "PI42 : Ressource systeme provisoirement insuffisante"

error = ETEBAC5, 202
	# CREATE : F.CREATE
	title = "PI42 : Ressource utilisateur provisoirement insuffisante"

error = ETEBAC5, 204
	# CREATE : F.CREATE
	title = "PI12 : Fichier existe d�j�"

error = ETEBAC5, 205
	# SELECT : F.SELECT
	title = "PI12 : Fichier inexistant"

error = ETEBAC5, 208
	# CREATE : F.CREATE
	title = "PI51 : Fichier trop vieux"

error = ETEBAC5, 211
	# ORF : F.OPEN
	title = "Ouverture de fichier impossible"

error = ETEBAC5, 212
	# CRF : F.CLOSE
	title = "Fermeture de fichier anormale"

error = ETEBAC5, 213
	# PESIT : F.CANCEL
	# WRITE : F.WRITE
	# READ : F.READ
	title = "Erreur d'Entr�e/Sortie bloquante"

error = ETEBAC5, 214
	# READ : F.READ
	# ACK-WRITE : F.WRITE
	title = "PI18 : Echec de n�gociation sur point de relance"

error = ETEBAC5, 217
	# DTF : F.RESTART F.ABORT F.CANCEL
	title = "Trop de point de synchronisation sans acquittement"

error = ETEBAC5, 218
	# RESYN : F.ABORT F.CANCEL
	title = "Resynchronisation impossible"

error = ETEBAC5, 219
	# DTF : F.CANCEL
	title = "Espace Fichier epuise"

error = ETEBAC5, 220
	# DTF : F.RESTART F.ABORT F.CANCEL
	title = "Longueur article incorrecte"
	action = "Verifier /PI75 et /PI32"

error = ETEBAC5, 223
	# TRANS-END : F.TRANSFER.END
	# CRF : F.CLOSE
	# DESELECT : F.DESELECT
	# ACK-TRANS-END : F.ABORT F.CLOSE
	title = "Fin de transfert anormale"

error = ETEBAC5, 224
	# TRANS-END : F.TRANSFER.END
	# ACK-TRANS-END : F.ABORT F.CLOSE
	title = "Taille du fichier transmis > taille annoncee"

error = ETEBAC5, 226
	# PESIT : F.CREATE F.SELECT
	title = "Refus de Transfert"

error = ETEBAC5, 228
	# CREATE : F.CREATE
	title = "PI11 : Type de fichier non support�"
	action = "Verifier : plus assure ou non encore assure"

error = ETEBAC5, 229
	# CREATE : F.CREATE
	title = "PI11 : Type de fichier incompatible avec sens du transfert"

error = ETEBAC5, 230
	# CREATE : F.CREATE
	title = "PI11 : Type de syntaxe incompatible avec nature d'application"

error = ETEBAC5, 231
	# CREATE : F.CREATE
	# ACK-CREATE : F.DESELECT
	title = "PI13 : Numero de transfert non unique"

error = ETEBAC5, 232
	# CREATE : F.CREATE
	title = "PI16 : Codage incompatible avec type de fichier"
	action = "Verifier le PI11 associe"

error = ETEBAC5, 233
	# SELECT : F.SELECT
	title = "PI15 : Contexte de relance non disponible"

error = ETEBAC5, 234
	# CREATE : F.CREATE
	# SELECT : F.SELECT
	# ACK-CREATE : F.DESELECT
	title = "PI25 : Incoherent avec taille article"

error = ETEBAC5, 235
	# CREATE : F.CREATE
	title = "PI31 : Format d'article incompatible avec type de fichier"
	action = "Verifier le PI11 associe"

error = ETEBAC5, 236
	# CREATE : F.CREATE
	title = "PI31 : Longueur d'article incompatible avec type de fichier"
	action = "Verifier le PI11 associe"

error = ETEBAC5, 237
	# CREATE : F.CREATE
	# SELECT : F.SELECT
	title = "PI61 : Idientifiant client incorrect"

error = ETEBAC5, 238
	# CREATE : F.CREATE
	# SELECT : F.SELECT
	title = "PI61 : Client non autoris�e"
	action = "Corriger"

error = ETEBAC5, 239
	# CREATE : F.CREATE
	# SELECT : F.SELECT
	title = "PI61 : Combinaison Client/Demandeur/service non autoris�e"

error = ETEBAC5, 240
	# CREATE : F.CREATE
	# SELECT : F.SELECT
	title = "PI61 : Client non autoris� sur ce serveur"
	action = "Appeler le serveur lie au SIRET mentionne"

error = ETEBAC5, 241
	# CREATE : F.CREATE
	# SELECT : F.SELECT
	title = "PI62 : Banque inconnue sur ce serveur"
	action = "Appeler le numero mentionne"

error = ETEBAC5, 242
	# CREATE : F.CREATE
	# SELECT : F.SELECT
	title = "PI63 : Ancien mot de passe invalide"

error = ETEBAC5, 243
	# CREATE : F.CREATE
	# SELECT : F.SELECT
	title = "PI63 : Nouveau mot de passe invalide"
	action = "Comparer Ancien et Nouveau mot de passe "

error = ETEBAC5, 244
	# ORF : F.OPEN
	title = "PI21 : Incompatibilit� compression / Chiffrement"

error = ETEBAC5, 245
	# DTF-END : F.ABORT F.CANCEL
	title = "Longueur fichier incorrecte"
	action = "Verifier /PI75"

error = ETEBAC5, 246
	# SELECT : F.SELECT
	title = "PI11 : Aucun de fichier de ce type pour ce client"
	action = "Verifier le contrat, ou pas de fichier ce jour"

error = ETEBAC5, 247
	# SELECT : F.SELECT
	title = "PI11 : Aucun fichier de ce type sur ce serveur"

error = ETEBAC5, 248
	# SELECT : F.SELECT
	title = "PI12 : Type d'identifcation non support�"

error = ETEBAC5, 249
	# SELECT : F.SELECT
	title = "PI12 : R�f�rence nominative non support�e"

error = ETEBAC5, 250
	# SELECT : F.SELECT
	title = "PI12 : Fichier d�j� transf�r�"

error = ETEBAC5, 251
	# SELECT : F.SELECT
	title = "PI12 : Type de reference non offert"
	action = "Verifier contrat et abonnement"

error = ETEBAC5, 252
	# SELECT : F.SELECT
	title = "PI12 : Date de d�but trop ant�rieure"

error = ETEBAC5, 253
	# SELECT : F.SELECT
	title = "PI12 : Date(s) incorrecte(s)"

error = ETEBAC5, 254
	# CREATE : F.CREATE
	# SELECT : F.SELECT
	title = "PI11 : Service Fichier ferme"
	action = "Attendre heure d'ouverture mentionnee"

error = ETEBAC5, 255
	# ACK-ORF : F.OPEN F.CLOSE F.ABORT
	title = "PI21 : Echec de negotiation de compression"

error = ETEBAC5, 256
	# ACK-SYN : F.RESTART F.CANCEL
	# ACK-SYN : F.RESTART F.CANCEL
	title = "PI20 : Numero de point de synchro errone"

error = ETEBAC5, 301
	# CONNECT : F.CONNECT F.ABORT
	title = "PI 4 : Identificateur serveur inconnu"

error = ETEBAC5, 304
	# CONNECT : F.CONNECT F.ABORT
	title = "PI 3 : Identificateur demandeur non autoris�"

error = ETEBAC5, 306
	# CONNECT : F.CONNECT F.ABORT
	# ACONNECT : F.RELEASE F.ABORT
	title = "PI23 : Echec de n�gociation Re-Synchro"

error = ETEBAC5, 307
	# CONNECT : F.CONNECT F.ABORT
	# ACONNECT : F.RELEASE F.ABORT
	title = "PI 7 : Echec de n�gociation Synchro"

error = ETEBAC5, 308
	# CONNECT : F.CONNECT F.ABORT
	# ACONNECT : F.RELEASE F.ABORT
	title = "PI 6 : Version ETEBAC 5 non supporte"
	action = "Prendre No de version mentionnee"

error = ETEBAC5, 311
	# PESIT : F.ABORT
	title = "Erreur de protocole PeSIT distant"

error = ETEBAC5, 312
	# RELEASE : F.RELEASE F.ABORT
	title = "Fermeture de service demande par l'utilisateur"

error = ETEBAC5, 313
	# PESIT : F.ABORT
	title = "Connexion rompue en fin d'inactivite TD"

error = ETEBAC5, 315
	# PESIT : F.ABORT
	title = "Echec de n�gociation"

error = ETEBAC5, 317
	# PESIT : F.ABORT
	title = "Ech�ance de temporisation"

error = ETEBAC5, 318
	# PESIT : F.ABORT
	title = "PI obligatoire absent ou contenu illicite d'un PI"

error = ETEBAC5, 319
	# PESIT : F.ABORT
	# TRANS-END : F.TRANSFER.END
	# ACK-TRANS-END : F.ABORT F.CLOSE
	title = "Nombre d'octects ou d'articles incorrect"
	action = "Verifier PI27(nb d'octects), PI28(nb d'articles)"

error = ETEBAC5, 320
	# PESIT : F.ABORT
	title = "Nombre excessif de resynchronisations pour un transfert"

error = ETEBAC5, 321
	# CONNECT : F.CONNECT F.ABORT
	title = "Appeler le numero de secours"
	action = "Utiliser le numero de secours"

error = ETEBAC5, 322
	# CONNECT : F.CONNECT F.ABORT
	title = "Appeler ulterieurement"
	action = "Utiliser l'heure de reappel (HHMM)"

error = ETEBAC5, 323
	# CONNECT : F.CONNECT F.ABORT
	title = "Imcompatibilite CRC/Mode de connexion"
	action = "Si PAD, PI1=1 sinon PI1=0"

error = ETEBAC5, 324
	# CONNECT : F.CONNECT F.ABORT
	title = "PI 3 : Identificateur demandeur incorrect"
	action = "Verifier Cle SIRET (/9 ou /14)"

error = ETEBAC5, 325
	# CONNECT : F.CONNECT F.ABORT
	# ACONNECT : F.RELEASE F.ABORT
	title = "PI 5 : Ancien mot de passe invalide"
	action = "Prend en compte le PI3"

error = ETEBAC5, 326
	# CONNECT : F.CONNECT F.ABORT
	title = "PI 5 : Nouveau mot de passe invalide"
	action = "Verifier si Nouveau != Ancien mot de passe"

error = ETEBAC5, 327
	# CONNECT, CREATE : F.CONNECT F.CREATE F.ABORT
	title = "PI22 : Acces en reception provisoirement ferme"

error = ETEBAC5, 328
	# CONNECT CREATE : F.CONNECT F.CREATE F.ABORT
	title = "PI22 : Acces en reception non assure"

error = ETEBAC5, 329
	# CONNECT : F.CONNECT F.ABORT
	# SELECT : F.SELECT
	title = "PI22 : Acces en emission provisoirement ferme"
	action = "Regarder FPDU CONNECT ou SELECT"

error = ETEBAC5, 330
	# CONNECT : F.CONNECT F.ABORT
	# SELECT : F.SELECT
	title = "PI22 : Acces en emission non assure"
	action = "Regarder FPDU CONNECT ou SELECT"

error = ETEBAC5, 331
	# CONNECT : F.CONNECT F.ABORT
	title = "PI26 : Valeur excessive de tempo"

error = ETEBAC5, 332
	# CREATE : F.ABORT
	title = "Ecriture non n�goci�e"

error = ETEBAC5, 333
	# SELECT : F.ABORT
	title = "Lecture non n�goci�e"

error = ETEBAC5, 334
	# CONNECT : F.ABORT
	title = "Refus de taxation au demand�"

error = ETEBAC5, 335
	# CONNECT : F.CONNECT F.ABORT
	title = "Numero d'appelant invalide"

error = ETEBAC5, 336
	# ACK-CREATE : F.DESELECT F.ABORT
	title = "Refus date et heure du serveur"

error = ETEBAC5, 400
	# CREATE : F.CREATE F.ABORT
	# SELECT : F.SELECT
	title = "PI71 : Niveau d'authentification insuffisant"
	action = "Verifier votre contrat"

error = ETEBAC5, 401
	# CREATE : F.ABORT
	# SELECT : F.SELECT
	title = "PI71 : Refus d'authentification avec ce demandeur"
	action = "Verifier votre contrat"

error = ETEBAC5, 402
	# CREATE : F.ABORT
	# SELECT : F.SELECT
	title = "PI71 : Authentification ETEBAC5 non supportee"

error = ETEBAC5, 403
	# CREATE : F.ABORT
	# SELECT : F.SELECT
	title = "PI71 : Algorithme d'authentification incorrecte"

error = ETEBAC5, 404
	# CREATE : F.ABORT
	# SELECT : F.SELECT
	title = "PI72 : Elements d'authentification incoparible avec PI71"

error = ETEBAC5, 405
	# ORF : F.ABORT
	# ACK-CREATE : F.DESELECT F.ABORT
	title = "PI72 : Echec d'authentification"
	action = "Verifier si (((ALEA2)Pd)Sd) == ALEA2"

error = ETEBAC5, 410
	# CREATE : F.CREATE F.ABORT
	# SELECT : F.SELECT
	title = "PI73 : Scellement ETEBAC 5 non support�"

error = ETEBAC5, 411
	# CREATE : F.CREATE F.ABORT
	# SELECT : F.SELECT
	title = "PI73 : Algorithme de Scellement non support�"

error = ETEBAC5, 412
	# CREATE : F.CREATE F.ABORT
	# SELECT : F.SELECT
	title = "PI73 : Mode op�ratoire non support� (Type scellement)"

error = ETEBAC5, 413
	# CREATE : F.CREATE F.ABORT
	# SELECT : F.SELECT
	title = "PI73 : Service de scellement non offert"

error = ETEBAC5, 414
	# CREATE : F.CREATE F.ABORT
	# SELECT : F.SELECT
	title = "PI73 : Mode de transfert du scellement non supporte"

error = ETEBAC5, 415
	# CREATE : F.CREATE F.ABORT
	# SELECT : F.SELECT
	title = "PI73 : Protection exigee des elements de scellement"

error = ETEBAC5, 416
	# CREATE : F.CREATE F.ABORT
	# SELECT : F.SELECT
	title = "PI73 : Scellement exige"

error = ETEBAC5, 417
	# ORF : F.OPEN F.ABORT
	# ACK-ORF : F.CLOSE F.ABORT
	title = "PI74 : Elements de scellement incorrect"

error = ETEBAC5, 418
	# ORF : F.OPEN F.ABORT
	# ACK-ORF : F.CLOSE F.ABORT
	title = "PI74 : Incompatibilite elements de scellement avec PI73"

error = ETEBAC5, 419
	# SYN : F.RESTART F.ABORT
	# ACK-SYN : F.RESTART F.CANCEL
	title = "Sceau partiel incorrect"
	action = "Se resynchroniser / dernier point de synchro"

error = ETEBAC5, 420
	# DTF-END : F.RESTART F.ABORT F.CANCEL
	title = "PI78 : Sceau incorrect"

error = ETEBAC5, 430
	# CREATE : F.CREATE F.ABORT
	# SELECT : F.SELECT
	title = "PI75 : Chiffrement ETEBAC 5 non supporte"

error = ETEBAC5, 431
	# CREATE : F.CREATE F.ABORT
	# SELECT : F.SELECT
	title = "PI75 : Algorithme de chiffrement non supporte"

error = ETEBAC5, 432
	# CREATE : F.CREATE F.ABORT
	# SELECT : F.SELECT
	title = "PI75 : Mode operatoire non supporte"

error = ETEBAC5, 433
	# CREATE : F.CREATE F.ABORT
	# SELECT : F.SELECT
	title = "PI75 : Service de chiffrement non offert"

error = ETEBAC5, 434
	# CREATE : F.CREATE F.ABORT
	# SELECT : F.SELECT
	title = "PI75 : Mode de transfert du chiffrement non supporte"

error = ETEBAC5, 435
	# CREATE : F.CREATE F.ABORT
	# SELECT : F.SELECT
	title = "PI75 : Protection exigee des elements de chiffrement"

error = ETEBAC5, 436
	# CREATE : F.CREATE F.ABORT
	# SELECT : F.SELECT
	title = "PI75 : Chiffrement exigee"

error = ETEBAC5, 437
	# ORF : F.OPEN F.ABORT
	# ACK-ORF : F.CLOSE F.ABORT
	title = "PI76 : Elements de chiffrement incorrects"
	action = "Verifier : redondance, longueur, parite"

error = ETEBAC5, 438
	# ORF : F.OPEN F.ABORT
	# ACK-ORF : F.CLOSE F.ABORT
	title = "PI76 : Incompatibilite elements de chiffrement avec PI75"

error = ETEBAC5, 450
	# CREATE : F.CREATE F.ABORT
	# SELECT : F.SELECT
	title = "PI77 : Signature ETEBAC 5 non supportee"

error = ETEBAC5, 451
	# CREATE : F.CREATE F.ABORT
	# SELECT : F.SELECT
	title = "PI77 : Algorithme de signature non supportee"

error = ETEBAC5, 452
	# CREATE : F.CREATE F.ABORT
	# SELECT : F.SELECT
	title = "PI77 : Mode operatoire non supporte"

error = ETEBAC5, 453
	# CREATE : F.CREATE F.ABORT
	# SELECT : F.SELECT
	title = "PI77 : Service de signature non offert"

error = ETEBAC5, 454
	# CREATE : F.CREATE F.ABORT
	# SELECT : F.SELECT
	title = "PI77 : Service de double signature non offert"

error = ETEBAC5, 455
	# CREATE : F.CREATE F.ABORT
	# SELECT : F.SELECT
	title = "PI77 : Signature exigee"

error = ETEBAC5, 456
	# CREATE : F.CREATE F.ABORT
	# SELECT : F.SELECT
	title = "PI77 : Double signature exigee"

error = ETEBAC5, 457
	# DTF-END : F.ABORT F.CANCEL
	title = "PI79 : Premiere signature incorrecte"

error = ETEBAC5, 458
	# DTF-END : F.ABORT F.CANCEL
	title = "PI79 : Premiere signature incompatible avec PI77"

error = ETEBAC5, 459
	# DTF-END : F.ABORT F.CANCEL
	title = "PI82 : Deuxieme signature incorrecte"

error = ETEBAC5, 460
	# DTF-END : F.ABORT F.CANCEL
	title = "PI82 : Deuxieme signature incompatible avec PI77"

error = ETEBAC5, 461
	# TRANS-END : F.TRANSFER.END
	title = "PI81 : Signature erronee"

error = ETEBAC5, 462
	# TRANS-END : F.TRANSFER.END
	title = "PI81 : Horodatage incorrect"

error = ETEBAC5, 470
	# CREATE : F.ABORT
	# SELECT : F.SELECT
	# ACK-CREATE : F.DESELECT F.ABORT
	title = "PI80 : Accr�ditation incompatible avec PI71"

error = ETEBAC5, 471
	# CREATE : F.ABORT
	# SELECT : F.SELECT
	# ACK-CREATE : F.DESELECT F.ABORT
	title = "PI80 : Type d'accr�ditation demandeur/serveur incorrect"
	action = "Verifier par rapport au PI71"

error = ETEBAC5, 472
	# CREATE : F.ABORT
	# SELECT : F.SELECT
	# ACK-CREATE : F.DESELECT F.ABORT
	title = "PI80 : Identificateur demandeur/serveur incorrect"

error = ETEBAC5, 473
	# CREATE : F.CREATE F.ABORT
	# SELECT : F.SELECT
	title = "PI80 : Demandeur non autoris�"
	action = "Verifier par rapport au contrat souscrit"

error = ETEBAC5, 474
	# CREATE : F.ABORT
	# SELECT : F.SELECT
	title = "PI80 : Opposition sur demandeur"

error = ETEBAC5, 475
	# CREATE : F.ABORT
	# SELECT : F.SELECT
	title = "PI80 : Opposition sur dispositif demandeur"

error = ETEBAC5, 476
	# CREATE : F.ABORT
	# SELECT : F.SELECT
	# ACK-CREATE : F.DESELECT F.ABORT
	title = "PI80 : Generation d'accreditation non supportee"

error = ETEBAC5, 477
	# CREATE : F.ABORT
	# SELECT : F.SELECT
	# ACK-CREATE : F.DESELECT F.ABORT
	title = "PI80 : Accr�ditation demandeur/serveur incorrecte"

error = ETEBAC5, 478
	# CREATE : F.ABORT
	# SELECT : F.SELECT
	# ACK-CREATE : F.DESELECT F.ABORT
	title = "PI80 : Mode test non supporte"
	action = "Verifier si le serveur ne supporte que le mode reel"

error = ETEBAC5, 479
	# CREATE : F.ABORT
	# SELECT : F.SELECT
	# ACK-CREATE : F.DESELECT F.ABORT
	title = "PI80 : Mode reel non supporte"
	action = "Verifier si le serveur ne supporte que le mode test"

error = ETEBAC5, 490
	# ORF : F.OPEN F.ABORT
	title = "PI80 : Type de premiere accreditation incompatible avec PI77"
	action = "Verifier par rapport au PI77"

error = ETEBAC5, 491
	# ORF : F.OPEN F.ABORT
	title = "PI80 : Type de premiere accreditation incorrect"
	action = "Verifier si type = 0 ou 2"

error = ETEBAC5, 492
	# ORF : F.OPEN F.ABORT
	title = "PI80 : Identificateur premier signataire incorrect"
	action = "Verifier cles SIRET et regle de structuration"

error = ETEBAC5, 493
	# ORF : F.OPEN F.ABORT
	title = "PI80 : Premier signataire non autorise"
	action = "Verifier par rapport au client et type de fichier"

error = ETEBAC5, 494
	# ORF : F.OPEN F.ABORT
	title = "PI80 : Opposition sur premier signataire"

error = ETEBAC5, 495
	# ORF : F.OPEN F.ABORT
	title = "PI80 : Opposition sur dispositif du premier signataire"

error = ETEBAC5, 496
	# ORF : F.OPEN F.ABORT
	title = "PI80 : Generation accreditation premier signataire non supportee"

error = ETEBAC5, 497
	# ORF : F.OPEN F.ABORT
	title = "PI80 : Incompatibilite premiere accreditation client / accreditation demandeur"
	action = "Verifier champs CREATE et ORF du PI80"

error = ETEBAC5, 498
	# ORF : F.OPEN F.ABORT
	title = "PI80 : Premi�re accr�ditaion incorrecte"
	action = "Verifier signature / aux elements en clair de l'accreditation"

error = ETEBAC5, 499
	title = "Erreur module de s�curit� RSA"

error = ETEBAC5, 500
	# ORF : F.OPEN F.ABORT
	title = "PI83 : Deuxieme accr�ditaion incompatible / PI77"
	action = "Verifier presence / PI77"

error = ETEBAC5, 501
	# ORF : F.OPEN F.ABORT
	title = "PI83 : Type deuxieme accr�ditaion incorrect"
	action = "Verifier si type == 0 ou 2"

error = ETEBAC5, 502
	# ORF : F.OPEN F.ABORT
	title = "PI83 : Identificateur deuxieme signataire incorrect"
	action = "Verifier cles et regles de structure"

error = ETEBAC5, 503
	# ORF : F.OPEN F.ABORT
	title = "PI83 : Deuxieme signataire non autorise"
	action = "Verifier par rapport au client et type de fichier"

error = ETEBAC5, 504
	# ORF : F.OPEN F.ABORT
	title = "PI83 : Opposition sur deuxieme signataire"

error = ETEBAC5, 505
	# ORF : F.OPEN F.ABORT
	title = "PI83 : Opposition sur dispositif du deuxieme signataire"

error = ETEBAC5, 506
	# ORF : F.OPEN F.ABORT
	title = "PI83 : Generation accreditation du deuxieme signataire non supportee"
	action = "Verifier aupres de la banque"

error = ETEBAC5, 507
	# ORF : F.OPEN F.ABORT
	title = "PI83 : Type 2ieme accreditation client incompatible /demandeur"
	action = "Verifier champs PI80.CREATE et PI80.ORF"

error = ETEBAC5, 508
	# ORF : F.OPEN F.ABORT
	title = "PI83 : Deuxieme accreditation incoorrecte"
	action = "Verifier signature / aux elements en clair de l'accreditation"

error = PI, 01
	title = "Utilisation d'un CRC"

error = PI, 02
	title = "Diagnostic"

error = PI, 03
	title = "Identificateur du demandeur"

error = PI, 04
	title = "Identificateur du serveur"

error = PI, 05
	title = "Controle d'acces"

error = PI, 06
	title = "Numero de version"

error = PI, 07
	title = "Option - Point de synchro"

error = PI, 11
	title = "Type de fichier"

error = PI, 12
	title = "Nom de fichier"

error = PI, 13
	title = "Identificateur du Transfert"

error = PI, 14
	title = "Attributs demandes"

error = PI, 15
	title = "Transfert relance"

error = PI, 16
	title = "Code donnees"

error = PI, 17
	title = "Priorite de transfert"

error = PI, 18
	title = "Point de relance"

error = PI, 19
	title = "Code fin de transfert"

error = PI, 20
	title = "Numero de point de synchronisation"

error = PI, 21
	title = "Compression"

error = PI, 22
	title = "Type d'acces"

error = PI, 23
	title = "Resynchronisation"

error = PI, 25
	title = "Taille Max. d'une entite de donnees multi-FPDU"

error = PI, 26
	title = "Temporisation de surveillance du protocole"

error = PI, 27
	title = "Nombre d'octets de donnees (obligatoire si acces PAD)"

error = PI, 28
	title = "Nombre d'articles (obligatoire si acces PAD)"

error = PI, 29
	title = "Complement de diagnostic"

error = PI, 31
	title = "Format d'article"

error = PI, 32
	title = "Longueur d'article"

error = PI, 33
	title = "Organisation du fichier"

error = PI, 37
	title = "Label du fichier"

error = PI, 38
	title = "Longueur de la cle"

error = PI, 39
	title = "Deplacement de la cle"

error = PI, 41
	title = "Valeur de l'unite de reservation physique"

error = PI, 42
	title = "Valeur maximale de reservation d'espace"

error = PI, 51
	title = "Date et heure de creation"

error = PI, 52
	title = "Date et heure de derniere extraction"

error = PI, 61
	title = "Identification client"

error = PI, 62
	title = "Identification banque"

error = PI, 63
	title = "Controle d'acces fichier"

error = PI, 64
	title = "Date et heure du serveur"

error = PI, 71
	title = "Type d'authentification"

error = PI, 72
	title = "Elements d'authentification"

error = PI, 73
	title = "Type de scellement"

error = PI, 74
	title = "Elements de scellement"

error = PI, 75
	title = "Type de chiffrement"

error = PI, 76
	title = "Elements de chiffrement"

error = PI, 77
	title = "Type de signature"

error = PI, 78
	title = "Sceau"

error = PI, 79
	title = "Signature"

error = PI, 80
	title = "Accreditation"

error = PI, 81
	title = "Accuse de reception de la signature"

error = PI, 82
	title = "Deuxieme signature"

error = PI, 83
	title = "Deuxieme accreditation"

error = PI, 99
	title = "Message libre"

error = UEBKCOM, 9000
	title = "Erreur de lecture du fichier SE1 ou SE2"
	action = "Verifier les droits sur ce fichier"

error = UEBKCOM, 9001
	title = "Code Type signature �rron�er SE1 ou SE2"
	action = "Verifier les droits sur ce fichier"

error = UEBKCOM, 9002
	title = "Absence de la signature 1 dans le fichier"
	action = "Renouveller l'op�ration de signature"

error = UEBKCOM, 9003
	title = "Absence de la signature 2 dans le fichier"
	action = "Renouveler l'operation de signaturee"

error = UEBKCOM, 9004
	title = "Incoherence Nombre Signature et nom fichier"
	action = "Renouveler l'operation de signaturee"

error = UEBKCOM, 9005
	title = "Incoherence nombre signature et nom fichier"
	action = "Renouveler l'operation de signaturee"

error = UEBKCOM, 9006
	title = "Fichier d�j� pret au transfert ETEBAC5chier"
	action = "Integration possible grace a l'option -f"

error = UEBKCOM, 9009
	title = "Erreur d'extension du fichier de donneehier"
	action = "Verifier coherence *.SEi et *.00i"

error = UEBKCOM, 9010
	title = "Probleme a l'ouverture du fichier de signature"
	action = "Verifier les droits d'acces *.00i"

error = UEBKCOM, 9011
	title = "Erreur lecture fichier de signature"
	action = "Verifier les droits d'acces au fichier"

error = UEBKCOM, 9012
	title = "Absence du fichier de donn�egnaturee signature"
	action = "Verifier si fichier .00i est pr�sent dans le r�pertoire demand�"

error = UEBKCOM, 9013
	title = "Erreur �criture des attributs etendus"
	action = "Verifier les droits sur le EAFILE.*"

error = UEBKCOM, 9014
	title = "Erreur longueur fichier de signature"
	action = "Verifier la taille du fichier 1319 octets"

error = XLAT, 900
	title = "Erreur acces fichier"

error = XLAT, 901
	title = "Segment non supporte"

error = XLAT, 902
	title = "Segment inconnu"

error = XLAT, 903
	title = "Erreur memoire"

error = XLAT, 904
	title = "Mauvais format EDIFACT du fichier"

error = XLAT, 905
	title = "Mauvaise longueur du fichier au format variable"

error = XLAT, 906
	title = "Segments incompatibles"

error = XLAT, 907
	title = "Mauvaise version"

error = XLAT, 910
	title = "Segment UNH absent"

error = XLAT, 911
	title = "Segment UNB absent"

error = XLAT, 912
	title = "Segment BUS absent"

error = XLAT, 913
	title = "Segment NAD1 absent"

error = XLAT, 914
	title = "Segment NAD2 absent"

error = XLAT, 915
	title = "Segment FII1 absent"

error = XLAT, 916
	title = "Segment FII2 absent"

error = XLAT, 917
	title = "Segment DTM1 absent"

error = XLAT, 920
	title = "Segment MOA absent"

error = XLAT, 924
	title = "Segment UNT absent"

error = XLAT, 925
	title = "Segment UNZ absent"

error = XLAT, 926
	title = "Segment BGM absent"

error = XLAT, 951
	title = "Code emetteur donneur d'ordre absent"

error = XLAT, 952
	title = "Code ville donneur d'ordre absent"

error = XLAT, 953
	title = "Code devise absent"

error = XLAT, 954
	title = "Banque Donneur d'Ordre (D/O) absent"

error = XLAT, 955
	title = "Agence Donneur d'Ordre (D/O) absent"

error = XLAT, 956
	title = "Compte Donneur d'Ordre (D/O) absent"

error = XLAT, 957
	title = "Nom Donneur d'Ordre (D/O) absent"

error = XLAT, 958
	title = "Date execution absente"

error = XLAT, 962
	title = "Code montant absent"

error = XLAT, 963
	title = "Code Nom Beneficiaire absent"

error = XLAT, 964
	title = "Code Domiciliation Beneficiaire absent"

error = XLAT, 974
	title = "Banque Beneficiaire absente"

error = XLAT, 975
	title = "Agence Beneficiaire absente"

error = XLAT, 976
	title = "Compte Beneficiaire absent"

error = CISAM, 100
	title = "Add a duplicated key"

error = CISAM, 101
	title = "File is not opened"

error = CISAM, 102
	title = "Bad Argument"

error = CISAM, 103
	title = "Value out of range"

error = CISAM, 104
	title = "Too many files"

error = CISAM, 105
	title = "File format is corrupted"

error = CISAM, 106
	title = "Must be open with exclusive access (Add Index)"

error = CISAM, 107
	title = "Record is locked"

error = CISAM, 108
	title = "Index is already defined"

error = CISAM, 109
	title = "Delete primary key"

error = CISAM, 110
	title = "Beginning or End of file"

error = CISAM, 111
	title = "Record not found"

error = CISAM, 112
	title = "No current record"

error = CISAM, 113
	title = "File locked exclusively"

error = CISAM, 114
	title = "Filename too long"

error = CISAM, 115
	title = "Lock file cannot be created"

error = CISAM, 116
	title = "Bad memory allocation"

error = CISAM, 117
	title = "Bad custom collating"

error = CISAM, 118
	title = "Cannot read log file record"

error = CISAM, 119
	title = "Bad transaction log file"

error = CISAM, 120
	title = "Cannot open transaction log file"

error = CISAM, 121
	title = "Cannot write to transaction file"

error = CISAM, 122
	title = "Not in transaction"

error = CISAM, 124
	title = "Beginning of transaction not found"

error = CISAM, 125
	title = "Cannot use Network File Server"

error = CISAM, 126
	title = "Bad record number"

error = CISAM, 127
	title = "No primary key"

error = CISAM, 128
	title = "No logging"

error = CISAM, 129
	title = "Too may users"

error = CISAM, 131
	title = "No free disk space"

error = CISAM, 132
	title = "Record too long"

error = CISAM, 133
	title = "Audit trail exists"

error = CISAM, 134
	title = "No more locks"

error = CISAM, 150
	title = "Demo limits have been exceeded"

error = CISAM, 153
	title = "Must be in ISMANULOCK mode"

error = CISAM, 171
	title = "Incompatible file format"

#
# Specific error list - AIX version
#
error = X25IBM, 200
	title = "X25AUTH"

error = X25IBM, 201
	title = "X25BADID"

error = X25IBM, 202
	title = "X25CALLED"

error = X25IBM, 203
	title = "X25CALLING"

error = X25IBM, 204
	title = "X25CAUSE"

error = X25IBM, 205
	title = "X25CTRUSE"

error = X25IBM, 206
	title = "X25INIT"

error = X25IBM, 207
	title = "X25INVFAC"

error = X25IBM, 208
	title = "X25INVMON"

error = X25IBM, 209
	title = "X25LINKUSE"

error = X25IBM, 210
	title = "X25LONG"

error = X25IBM, 211
	title = "X25NAMEUSED"

error = X25IBM, 212
	title = "X25NOACK"

error = X25IBM, 213
	title = "X25NOCARD"

error = X25IBM, 214
	title = "X25NOCTR"

error = X25IBM, 215
	title = "X25NODATA"

error = X25IBM, 216
	title = "X25NODEVICE"

error = X25IBM, 217
	title = "X25NOIPC"

error = X25IBM, 218
	title = "X25NOLINK"

error = X25IBM, 219
	title = "X25NONAME"

error = X25IBM, 220
	title = "X25NOROUTER"

error = X25IBM, 221
	title = "X25NOTPVC"

error = X25IBM, 222
	title = "X25PGRP"

error = X25IBM, 223
	title = "X25PROTOCOL"

error = X25IBM, 224
	title = "X25PVCUSED"

error = X25IBM, 225
	title = "X25RECEIVERINIT"

error = X25IBM, 226
	title = "X25RESETCLEAR"

error = X25IBM, 227
	title = "X25ROUTERINIT"

error = X25IBM, 228
	title = "X25TABLE"

error = X25IBM, 229
	title = "X25TIMEOUT"

error = X25IBM, 230
	title = "X25TRUNC"

error = X25IBM, 231
	title = "X25TOOBIG"

error = X25IBM, 232
	title = "X25TOOMANYVCS"

error = X25IBM, 251
	title = "X25AUTHCTR"

error = X25IBM, 252
	title = "X25AUTHLISTEN"

error = X25IBM, 253
	title = "X25BADCONNID"

error = X25IBM, 254
	title = "X25BADDEVICE"

error = X25IBM, 255
	title = "X25BADLISTENID"

error = X25IBM, 256
	title = "X25INVCTR"

error = X25IBM, 257
	title = "X25LINKUP"

error = X25IBM, 258
	title = "X25LONGCUD"

error = X25IBM, 259
	title = "X25MAXDEVICE"

error = X25IBM, 260
	title = "X25MONITOR"

error = X25IBM, 261
	title = "X25NOACKREQ"

error = X25IBM, 262
	title = "X25NOSUCHLINK"

error = X25IBM, 263
	title = "X25NOTINIT"

error = X25IBM, 264
	title = "X25TRUNCRX"

error = X25IBM, 265
	title = "X25TRUNCTX"

error = X25IBM, 266
	title = "X25BADSTATUS"

error = X25IBM, 267
	title = "X25SYSERR"

