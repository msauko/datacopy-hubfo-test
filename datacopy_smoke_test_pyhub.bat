@echo off

:: ===================== Setting variables ================================== ::

setlocal ENABLEDELAYEDEXPANSION

SET PATH=%PATH%;D:\app\MongoDB\Server\3.2\bin;d:\app\gnupg


set COMMON_DIR=d:\kyriba\pyhub\common
set HUPLATFORM_DIR=d:\app\scripts\huplatform
set JAVA_HOME=d:\app\java
set RUN_DIRECTORY=.
for /f "tokens=1,2 delims== " %%a IN ('findstr /c:"DB_CONNECTION_STRING" %HUPLATFORM_DIR%\config.py') do (
set DB_CONNECTION_STRING=%%b
)
for /f "tokens=1,2 delims==" %%a IN ('findstr /c:"mongo.hosts" %COMMON_DIR%\datacopy.properties') do (
set MongoDB__HOST_IP=%%b
)
for /f "tokens=1,2 delims==" %%a IN ('findstr /c:"host" %COMMON_DIR%\datacopy.properties') do (
set OracleDB_HOST_IP=%%b
)
for /f "tokens=1,2 delims==" %%a IN ('findstr /c:"export.data.dir" %COMMON_DIR%\datacopy.properties') do (
set EXPORT_DATA_DIR=%%b
)
set MongoDB__PORT=27017
set MongoDB__USER=hub
set MongoDB__PASSWORD=hub
set MongoDB__DATABASE=hub
set MongoDB__AUTH_DATABASE=hub
set MongoDB__AUTH_MECHANISM=SCRAM-SHA-1
set COLLECTIONS_ARRAY=foCounters,foCounters.HSBC
set CUSTOMER_ARRAY="304379842,KITS"
set GPG_KEY_ARRAY="key_KITS-1.asc,2A68D9340F6277BAB484A8224FB7368E16260693","key_KITS-2.asc,AA56E7DDE91D3A080F9F5B81F61C2C548BBADC46","key_KITS-3.asc,CE6A9BC8669CCFFEE06473836DA5620B5E1A1456","key_AST.asc,F402271947397BFEEF5280D6F6846B3CF281646B","key_INTEGRATION.asc,8600687535F5A01C68CA586EE0300314D8A5734F","key_BERMUDA.asc,9E8B3F8160ED85827000E00BA3C239901AA2E94D"
set GPG_SECRET_KEY_ARRAY="secret_key_KITS-1.asc,2A68D9340F6277BAB484A8224FB7368E16260693","secret_key_KITS-2.asc,AA56E7DDE91D3A080F9F5B81F61C2C548BBADC46","secret_key_KITS-3.asc,CE6A9BC8669CCFFEE06473836DA5620B5E1A1456","secret_key_AST.asc,F402271947397BFEEF5280D6F6846B3CF281646B","secret_key_INTEGRATION.asc,8600687535F5A01C68CA586EE0300314D8A5734F","secret_key_BERMUDA.asc,9E8B3F8160ED85827000E00BA3C239901AA2E94D"
set GPG_FINGERPRINT=2A68D9340F6277BAB484A8224FB7368E16260693 AA56E7DDE91D3A080F9F5B81F61C2C548BBADC46 CE6A9BC8669CCFFEE06473836DA5620B5E1A1456 F402271947397BFEEF5280D6F6846B3CF281646B 8600687535F5A01C68CA586EE0300314D8A5734F 9E8B3F8160ED85827000E00BA3C239901AA2E94D 
set BIN_DIRECTORY=d:\kyriba\pyhub\current\bin\
set TARGET_SCHEMA=KVDEV
set USER=KYR_ADMIN
set PASSWORD=KYRIBA
set PORT=1521
set PYHUB_SID=XE

SET DATA_PATH=d:\data
SET GNUPG_PATH=d:\app\gnupg


set TEST_HOME_DIR=d:\tests\datacopy
set SCRIPTS_DIR=%TEST_HOME_DIR%\scripts
set TEMP1=%TEST_HOME_DIR%\temp\original
set TEMP2=%TEST_HOME_DIR%\temp\modified
set ETALON_DATA_DIR=%TEST_HOME_DIR%\ETALON_DATA
set FILES_ETALON_DATA_DIR=%ETALON_DATA_DIR%\FILES
set GPG_ETALON_DATA_DIR=%ETALON_DATA_DIR%\GPG
set JSON_ETALON_DATA_DIR=%ETALON_DATA_DIR%\JSON
set ETALON_AFTER_CUSTOMER_EXPORT=%FILES_ETALON_DATA_DIR%\files_after_customer_export\KITS
set ETALON_AFTER_SYSTEM_EXPORT=%FILES_ETALON_DATA_DIR%\files_after_system_export\SYSTEM
set ETALON_AFTER_CUSTOMER_DELETION=%FILES_ETALON_DATA_DIR%\files_after_customer_delete
set ETALON_AFTER_CUSTOMER_IMPORT=%FILES_ETALON_DATA_DIR%\files_after_customer_import
set ETALON_AFTER_CUSTOMER_IMPORT_SCENARIO_2=%FILES_ETALON_DATA_DIR%\files_after_customer_import_scenario2
set ETALON_AFTER_CUSTOMER_IMPORT_SCENARIO_3=%FILES_ETALON_DATA_DIR%\files_after_customer_import_scenario3
set ETALON_AFTER_CUSTOMER_IMPORT_SCENARIO_4=%FILES_ETALON_DATA_DIR%\files_after_customer_import_scenario4
set ETALON_BEFORE_DATACOPY=%FILES_ETALON_DATA_DIR%\original_files
set ETALON_BEFORE_DATACOPY_GPG=%GPG_ETALON_DATA_DIR%\gpg_keys_original
set SCENARIO_2_DATA=%FILES_ETALON_DATA_DIR%\exported_json_scenario2
set SCENARIO_3_DATA=%FILES_ETALON_DATA_DIR%\exported_json_scenario3
set SCENARIO_4_DATA=%FILES_ETALON_DATA_DIR%\exported_json_scenario4

:: ===================== Functions ================================== ::

:prepare_system

echo "-------------- Activation of GPG is STARTING"
regedit /S %GNUPG_PATH%\gnupg-w32.reg
echo "-------------- Activation of GPG is COMPLETED"


:drop_all_collections
echo "-------------- Action "Drop all collections" is STARTING"
del /q %SCRIPTS_DIR%\mongo_drop_all.js
echo use %MongoDB__DATABASE% > %SCRIPTS_DIR%\mongo_drop_all.js

for %%x in (%COLLECTIONS_ARRAY%) do (

echo db.%%x.drop(^) >> %SCRIPTS_DIR%\mongo_drop_all.js
)

mongo --host %MongoDB__HOST_IP% --port %MongoDB__PORT% -u %MongoDB__USER% -p %MongoDB__PASSWORD% --authenticationDatabase=%MongoDB__AUTH_DATABASE% --authenticationMechanism=%MongoDB__AUTH_MECHANISM% < %SCRIPTS_DIR%\mongo_drop_all.js


echo "-------------- Action "Drop all collections" is COMPLETED"



:import_fo_counters_json_to_mongodb
echo "---------- JSON files generation is starting"


cd /D %SCRIPTS_DIR%
rmdir /s /q %DATA_PATH%\foCounters


python Generate_fo_counters_collections.py

echo "---------- JSON files generation is completed"

echo "Mongo host ip address=%MongoDB__HOST_IP%"

echo "---------- Import of mongo db collections is starting"
for %%x in (foCounters foCounters.HSBC) do (
mongoimport --host %MongoDB__HOST_IP% --port %MongoDB__PORT% -u %MongoDB__USER% -p %MongoDB__PASSWORD% --authenticationDatabase=%MongoDB__AUTH_DATABASE% --authenticationMechanism=%MongoDB__AUTH_MECHANISM% --db %MongoDB__DATABASE% --collection %%x --file %SCRIPTS_DIR%\foCounters\%%x.json
)
echo "---------- Import of mongo db collections is completed"


:prepare_test_data
cd /D %TEST_HOME_DIR%



echo "-------------- Clean up target directories is STARTING"
rmdir /s /q %DATA_PATH%\keys
rmdir /s /q %DATA_PATH%\Tables
rmdir /s /q %DATA_PATH%\bkp_split
rmdir /s /q %DATA_PATH%\DB
rmdir /s /q %DATA_PATH%\Kanban\kStaIn
rmdir /s /q %EXPORT_DATA_DIR%\log
rmdir /s /q %EXPORT_DATA_DIR%\pyhub
rmdir /s /q %TEST_HOME_DIR%\temp
rmdir /s /q %TEST_HOME_DIR%\ETALON_DATA


echo "-------------- Clean up GPG keys is STARTING"
for %%f in (%GPG_FINGERPRINT%) do (
gpg --batch --fingerprint --yes --delete-secret-key %%f
gpg --batch --yes --delete-key %%f
)
echo "-------------- Import of GPG keys is COMPLETED"


echo "-------------- Clean up target directories is COMPLETED"

"D:\app\7-Zip\7z.exe" x "%TEST_HOME_DIR%\ETALON_DATA.zip" -o%TEST_HOME_DIR% -y

echo "-------------- Copy test data is STARTING"
xcopy /s /e /y /f %ETALON_BEFORE_DATACOPY%\keys %DATA_PATH%\keys\
xcopy /s /e /y /f %ETALON_BEFORE_DATACOPY%\Tables %DATA_PATH%\Tables\
xcopy /s /e /y /f %ETALON_BEFORE_DATACOPY%\bkp_split %DATA_PATH%\bkp_split\
xcopy /s /e /y /f %ETALON_BEFORE_DATACOPY%\DB %DATA_PATH%\DB\
xcopy /s /e /y /f %ETALON_BEFORE_DATACOPY%\kStaIn %DATA_PATH%\Kanban\kStaIn\
echo "-------------- Copy test data is COMPLETED"

echo "-------------- Import of GPG keys is STARTING"
echo mkdir %DATA_PATH%\keys\gnupg 
mkdir %DATA_PATH%\keys\gnupg 2>&1

echo "GPG_KEY_ARRAY:" %GPG_KEY_ARRAY%
for %%x in (%GPG_KEY_ARRAY%) do (
  echo "importing: " %%x
  for /f "tokens=1-2 delims=," %%a in (%%x) do (
    gpg --import %ETALON_BEFORE_DATACOPY_GPG%\%%a
  )
)

for %%x in (%GPG_SECRET_KEY_ARRAY%) do (
  for /f "tokens=1-2 delims=," %%a in (%%x) do (
    gpg --import %ETALON_BEFORE_DATACOPY_GPG%\%%a
  )
)

echo "-------------- Imported GPG keys are:"
gpg --list-keys
echo "-------------- Imported GPG secret-keys are:"
gpg --list-secret-keys
echo "-------------- Import of GPG keys is COMPLETED"


:export_mongo_db_collections_before_export
echo "---------- Export of mongo db collections before export is starting"

rmdir /s /q %JSON_ETALON_DATA_DIR%\beforeExport

mongoexport --host %MongoDB__HOST_IP% --port %MongoDB__PORT% -u %MongoDB__USER% -p %MongoDB__PASSWORD% --authenticationDatabase=%MongoDB__AUTH_DATABASE% --authenticationMechanism=%MongoDB__AUTH_MECHANISM% --db %MongoDB__DATABASE% --collection foCounters --query "{customer: null}" --out %JSON_ETALON_DATA_DIR%\beforeExport\null\foCounters_null.json --jsonArray

for %%x in (foCounters foCounters.HSBC) do (
for %%y in (KITS AST BERMUDA INTEGRATION FAKITS FASMOKE CGA01 ) do (
mongoexport --host %MongoDB__HOST_IP% --port %MongoDB__PORT% -u %MongoDB__USER% -p %MongoDB__PASSWORD% --authenticationDatabase=%MongoDB__AUTH_DATABASE% --authenticationMechanism=%MongoDB__AUTH_MECHANISM% --db %MongoDB__DATABASE% --collection %%x --query "{customer: '%%y'}" --out %JSON_ETALON_DATA_DIR%\beforeExport\%%y\%%x_%%y.json --jsonArray
)
)
echo "---------- Export of mongo db collections before export is completed"


:export_system_data_dry_run
echo "-------------- Dry-run of /datacopy_pyhub export -m system/ is STARTING"
cd /D %BIN_DIRECTORY%
type %COMMON_DIR%\datacopy.properties

cmd /c datacopy_pyhub.cmd export -m system --dry-run
if not errorlevel 1 (echo "-------------- Dry-run of /datacopy_pyhub export -m system/ is COMPLETED with Exit Code=!ERRORLEVEL!") else (echo "ERROR -------------- Dry-run of /datacopy_pyhub export -m system/ is FAILED with Exit Code=!ERRORLEVEL!" 
EXIT /B !ERRORLEVEL! 
) 
)

:validation_of_export_system_data_dry_run
if exist "%EXPORT_DATA_DIR%\pyhub\SYSTEM\" (
    echo "ERROR -------------- Validation of export system data dry-run is failed!"
	echo "Folder %EXPORT_DATA_DIR%\pyhub\SYSTEM is found after execution of system data export dry-run"
	exit /b 2
) else ( 
    echo "-------------- Folder %EXPORT_DATA_DIR%\pyhub\SYSTEM is not found after execution of system data export dry-run. Validation passed"
)
)




:export_system_data
echo "-------------- Export of system data /datacopy_pyhub export -m system/ is STARTING"
cd /D %BIN_DIRECTORY%
type %COMMON_DIR%\datacopy.properties

cmd /c datacopy_pyhub.cmd export -m system 
if not errorlevel 1 (echo "-------------- Export of system data /datacopy_pyhub export -m system/ is COMPLETED with Exit Code=!ERRORLEVEL!") else (echo "ERROR -------------- Export of system data /datacopy_pyhub export -m system/ is FAILED with Exit Code=!ERRORLEVEL!" 
EXIT /B !ERRORLEVEL! 
) 
)


:export_mongo_db_collections_after_system_data_export
echo "---------- Export of mongo db collections after system data export is starting"

rmdir /s /q %JSON_ETALON_DATA_DIR%\AfterSystemDataExport

mongoexport --host %MongoDB__HOST_IP% --port %MongoDB__PORT% -u %MongoDB__USER% -p %MongoDB__PASSWORD% --authenticationDatabase=%MongoDB__AUTH_DATABASE% --authenticationMechanism=%MongoDB__AUTH_MECHANISM% --db %MongoDB__DATABASE% --collection foCounters --query "{customer: null}" --out %JSON_ETALON_DATA_DIR%\AfterSystemDataExport\null\foCounters_null.json --jsonArray

for %%x in (foCounters foCounters.HSBC) do (
for %%y in (KITS AST BERMUDA INTEGRATION FAKITS FASMOKE CGA01 ) do (
mongoexport --host %MongoDB__HOST_IP% --port %MongoDB__PORT% -u %MongoDB__USER% -p %MongoDB__PASSWORD% --authenticationDatabase=%MongoDB__AUTH_DATABASE% --authenticationMechanism=%MongoDB__AUTH_MECHANISM% --db %MongoDB__DATABASE% --collection %%x --query "{customer: '%%y'}" --out %JSON_ETALON_DATA_DIR%\AfterSystemDataExport\%%y\%%x_%%y.json --jsonArray
)
)
echo "---------- Export of mongo db collections after system data export is completed"





:validation_of_system_data_export

FOR /F "tokens=* USEBACKQ" %%S IN (`dir /b %EXPORT_DATA_DIR%\pyhub\SYSTEM`) DO (
SET SYS_NUM_FOLDER=%%S
)
ECHO %SYS_NUM_FOLDER%

FOR %%i IN (backup.json kanban.json keys.json mongo.json tables.json) DO (
echo %%i

echo n|comp "%EXPORT_DATA_DIR%\pyhub\SYSTEM\%SYS_NUM_FOLDER%\%%i" "%ETALON_AFTER_SYSTEM_EXPORT%\%%i"
if not errorlevel 1 (echo "-------------- Comparison after system data export passed for %%i with Exit Code=!ERRORLEVEL!") else (echo "ERROR -------------- Comparison after system data export FAILED for %%i with Exit Code=!ERRORLEVEL!" 
EXIT /B !ERRORLEVEL! 
)
)

mkdir %SCENARIO_4_DATA%\SYSTEM
xcopy /E %EXPORT_DATA_DIR%\pyhub\SYSTEM %SCENARIO_4_DATA%\SYSTEM


rmdir /s /q %TEMP1%\
rmdir /s /q %TEMP2%\
mkdir %TEMP1%
mkdir %TEMP2%

xcopy /s /e /y /f %ETALON_BEFORE_DATACOPY%\keys %TEMP1%\keys\
xcopy /s /e /y /f %ETALON_BEFORE_DATACOPY%\Tables %TEMP1%\Tables\
xcopy /s /e /y /f %ETALON_BEFORE_DATACOPY%\bkp_split %TEMP1%\bkp_split\
xcopy /s /e /y /f %ETALON_BEFORE_DATACOPY%\DB %TEMP1%\DB\
xcopy /s /e /y /f %ETALON_BEFORE_DATACOPY%\kStaIn %TEMP1%\Kanban\kStaIn\

xcopy /s /e /y /f %DATA_PATH%\keys %TEMP2%\keys\
xcopy /s /e /y /f %DATA_PATH%\Tables %TEMP2%\Tables\
xcopy /s /e /y /f %DATA_PATH%\bkp_split %TEMP2%\bkp_split\
xcopy /s /e /y /f %DATA_PATH%\DB %TEMP2%\DB\
xcopy /s /e /y /f %DATA_PATH%\Kanban\kStaIn %TEMP2%\Kanban\kStaIn\


cd /D %TEMP1%
for /r %%d in (*) do copy "%%d" "%TEMP1%"

cd /D %TEMP2%
for /r %%d in (*) do copy "%%d" "%TEMP2%"

cd /D %TEMP1%

FOR %%i IN (*) DO (
echo %%i


echo n|comp "%TEMP1%\%%i" "%TEMP2%\%%i" 
if not errorlevel 1 (echo "%%i is OK") else (echo "ERROR -------------- Comparison after system data export FAILED for %%i with Exit Code=!ERRORLEVEL!" 
EXIT /B !ERRORLEVEL! 
) 
)

echo "-------------- Comparison of JSON after system data export is STARTING" 


echo n|comp "%JSON_ETALON_DATA_DIR%\beforeExport\null\foCounters_null.json" "%JSON_ETALON_DATA_DIR%\AfterSystemDataExport\null\foCounters_null.json"
if not errorlevel 1 (echo "foCounters_null.json is OK") else (echo "ERROR -------------- Comparison after system data export FAILED for foCounters_null.json with Exit Code=!ERRORLEVEL!" 
EXIT /B !ERRORLEVEL! 
) 
)

for %%x in (foCounters foCounters.HSBC) do (
for %%y in (KITS AST BERMUDA INTEGRATION FAKITS FASMOKE CGA01) do (
echo n|comp "%JSON_ETALON_DATA_DIR%\beforeExport\%%y\%%x_%%y.json" "%JSON_ETALON_DATA_DIR%\AfterSystemDataExport\%%y\%%x_%%y.json"
if not errorlevel 1 (echo "%%x_%%y.json is OK") else (echo "ERROR -------------- Comparison after system data export FAILED for %%x_%%y.json with Exit Code=!ERRORLEVEL!" 
EXIT /B !ERRORLEVEL! 
) 
)
)
)

echo "-------------- Comparison of JSON after system data export is COMPLETED"





:export_customer_data_dry_run
cd /D %BIN_DIRECTORY%

echo mkdir %DATA_PATH%\keys\gnupg 
mkdir %DATA_PATH%\keys\gnupg


for %%x in (%CUSTOMER_ARRAY%) do (
  for /f "tokens=1-2 delims=," %%a in (%%x) do (
    echo %%b %%a 
	echo "-------------- Dry-run of /datacopy_pyhub export -m customer --code %%b --ddu %%a/ is STARTING"
    cmd /c datacopy_pyhub.cmd export -m customer --code %%b --ddu %%a --dry-run
	if not errorlevel 1 (echo "-------------- Dry-run of /datacopy_pyhub export -m customer --code %%b --ddu %%a/ is COMPLETED with Exit Code=!ERRORLEVEL!") else (echo "ERROR -------------- Dry-run of /datacopy_pyhub export -m customer --code %%b --ddu %%a/ is FAILED with Exit Code=!ERRORLEVEL!" 
    EXIT /B !ERRORLEVEL! 
    ) 
  )
)

:validation_of_export_customer_data_dry_run

if exist "%EXPORT_DATA_DIR%\pyhub\KITS" (
    echo "ERROR -------------- Validation of export customer data dry-run is failed"
	echo "Folder %EXPORT_DATA_DIR%\pyhub\KITS is found after execution of customer data export dry-run"
	exit /b 2
) else ( 
    echo "-------------- Folder %EXPORT_DATA_DIR%\pyhub\KITS is not found after execution of customer data export dry-run. Validation passed"
)

:export_customer_data
cd /D %BIN_DIRECTORY%

for %%x in (%CUSTOMER_ARRAY%) do (
  for /f "tokens=1-2 delims=," %%a in (%%x) do (
    echo %%b %%a 
	echo "-------------- Export of customer data /datacopy_pyhub export -m customer --code %%b --ddu %%a/ is STARTING"
    cmd /c datacopy_pyhub.cmd export -m customer --code %%b --ddu %%a
	if not errorlevel 1 (echo "-------------- Export of customer data /datacopy_pyhub export -m customer --code %%b --ddu %%a/ is COMPLETED with Exit Code=!ERRORLEVEL!") else (echo "ERROR -------------- Export of customer data /datacopy_pyhub export -m customer --code %%b --ddu %%a/ is FAILED with Exit Code=!ERRORLEVEL!" 
    EXIT /B !ERRORLEVEL! 
    ) 
  )
)


:export_mongo_db_collections_after_customer_data_export
echo "---------- Export of mongo db collections after customer data export is starting"

rmdir /s /q %JSON_ETALON_DATA_DIR%\AfterCustomerDataExport

mongoexport --host %MongoDB__HOST_IP% --port %MongoDB__PORT% -u %MongoDB__USER% -p %MongoDB__PASSWORD% --authenticationDatabase=%MongoDB__AUTH_DATABASE% --authenticationMechanism=%MongoDB__AUTH_MECHANISM% --db %MongoDB__DATABASE% --collection foCounters --query "{customer: null}" --out %JSON_ETALON_DATA_DIR%\AfterCustomerDataExport\null\foCounters_null.json --jsonArray

for %%x in (foCounters foCounters.HSBC) do (
for %%y in (KITS AST BERMUDA INTEGRATION FAKITS FASMOKE CGA01 ) do (
mongoexport --host %MongoDB__HOST_IP% --port %MongoDB__PORT% -u %MongoDB__USER% -p %MongoDB__PASSWORD% --authenticationDatabase=%MongoDB__AUTH_DATABASE% --authenticationMechanism=%MongoDB__AUTH_MECHANISM% --db %MongoDB__DATABASE% --collection %%x --query "{customer: '%%y'}" --out %JSON_ETALON_DATA_DIR%\AfterCustomerDataExport\%%y\%%x_%%y.json --jsonArray
)
)
echo "---------- Export of mongo db collections after customer data export is completed"


:validation_of_customer_export

FOR /F "tokens=* USEBACKQ" %%C IN (`dir /b %EXPORT_DATA_DIR%\pyhub\KITS`) DO (
SET CUST_NUM_FOLDER=%%C
)
ECHO %CUST_NUM_FOLDER%

FOR %%i IN (backup.json kanban.json keys.json tables.json) DO (
echo %%i

echo n|comp "%EXPORT_DATA_DIR%\pyhub\KITS\%CUST_NUM_FOLDER%\%%i" "%ETALON_AFTER_CUSTOMER_EXPORT%\%%i"
if not errorlevel 1 (echo "-------------- Comparison after customer data export passed for %%i with Exit Code=!ERRORLEVEL!") else (echo "ERROR -------------- Comparison after customer data export FAILED for %%i with Exit Code=!ERRORLEVEL!" 
EXIT /B !ERRORLEVEL! 
)
)

mkdir %SCENARIO_4_DATA%\KITS
xcopy /E %EXPORT_DATA_DIR%\pyhub\KITS %SCENARIO_4_DATA%\KITS



rmdir /s /q %TEMP1%\
rmdir /s /q %TEMP2%\
mkdir %TEMP1%
mkdir %TEMP2%

xcopy /s /e /y /f %ETALON_BEFORE_DATACOPY%\keys %TEMP1%\keys\
xcopy /s /e /y /f %ETALON_BEFORE_DATACOPY%\Tables %TEMP1%\Tables\
xcopy /s /e /y /f %ETALON_BEFORE_DATACOPY%\bkp_split %TEMP1%\bkp_split\
xcopy /s /e /y /f %ETALON_BEFORE_DATACOPY%\DB %TEMP1%\DB\
xcopy /s /e /y /f %ETALON_BEFORE_DATACOPY%\kStaIn %TEMP1%\Kanban\kStaIn\

xcopy /s /e /y /f %DATA_PATH%\keys %TEMP2%\keys\
xcopy /s /e /y /f %DATA_PATH%\Tables %TEMP2%\Tables\
xcopy /s /e /y /f %DATA_PATH%\bkp_split %TEMP2%\bkp_split\
xcopy /s /e /y /f %DATA_PATH%\DB %TEMP2%\DB\
xcopy /s /e /y /f %DATA_PATH%\Kanban\kStaIn %TEMP2%\Kanban\kStaIn\


cd /D %TEMP1%
for /r %%d in (*) do copy "%%d" "%TEMP1%"

cd /D %TEMP2%
for /r %%d in (*) do copy "%%d" "%TEMP2%"

cd /D %TEMP1%

FOR %%i IN (*) DO (
echo %%i


echo n|comp "%TEMP1%\%%i" "%TEMP2%\%%i" 
if not errorlevel 1 (echo "%%i is OK") else (echo "ERROR -------------- Comparison after customer data export FAILED for %%i with Exit Code=!ERRORLEVEL!" 
EXIT /B !ERRORLEVEL! 
) 
)

echo "-------------- Comparison of JSON after customer data export is STARTING" 


echo n|comp "%JSON_ETALON_DATA_DIR%\beforeExport\null\foCounters_null.json" "%JSON_ETALON_DATA_DIR%\AfterCustomerDataExport\null\foCounters_null.json"
if not errorlevel 1 (echo "foCounters_null.json is OK") else (echo "ERROR -------------- Comparison after customer data export FAILED for foCounters_null.json with Exit Code=!ERRORLEVEL!" 
EXIT /B !ERRORLEVEL! 
) 
)

for %%x in (foCounters foCounters.HSBC) do (
for %%y in (KITS AST BERMUDA INTEGRATION FAKITS FASMOKE CGA01) do (
echo n|comp "%JSON_ETALON_DATA_DIR%\beforeExport\%%y\%%x_%%y.json" "%JSON_ETALON_DATA_DIR%\AfterCustomerDataExport\%%y\%%x_%%y.json"
if not errorlevel 1 (echo "%%x_%%y.json is OK") else (echo "ERROR -------------- Comparison after customer data export FAILED for %%x_%%y.json with Exit Code=!ERRORLEVEL!" 
EXIT /B !ERRORLEVEL! 
) 
)
)
)

echo "-------------- Comparison of JSON after customer data export is COMPLETED"



:inactivate_customer
echo "-------------- Inactivation of customer is STARTING"
del /q %SCRIPTS_DIR%\inactivate_customer.sql

echo SET ECHO ON; > %SCRIPTS_DIR%\inactivate_customer.sql

for %%x in (%CUSTOMER_ARRAY%) do (
  for /f "tokens=1-2 delims=," %%a in (%%x) do (
    echo UPDATE %TARGET_SCHEMA%.CORPORATE_CUSTOMER SET IS_ACTIVE = '0' WHERE INTERFACE_CODE = '%%b'; >> %SCRIPTS_DIR%\inactivate_customer.sql
	echo SELECT INTERFACE_CODE, IS_ACTIVE from %TARGET_SCHEMA%.CORPORATE_CUSTOMER WHERE INTERFACE_CODE = '%%b'; >> %SCRIPTS_DIR%\inactivate_customer.sql
  )
)

echo EXIT >> %SCRIPTS_DIR%\inactivate_customer.sql

sqlplus %DB_CONNECTION_STRING% @%SCRIPTS_DIR%\inactivate_customer.sql

echo "-------------- Inactivation of customer is COMPLETED"


:delete_customer_dry_run
cd /D %BIN_DIRECTORY%

for %%x in (%CUSTOMER_ARRAY%) do (
  for /f "tokens=1-2 delims=," %%a in (%%x) do (
    echo %%b %%a 
	echo "-------------- Dry-run of /datacopy_pyhub delete -m customer --code %%b --ddu %%a/ is STARTING"
    cmd /c datacopy_pyhub.cmd delete -m customer --code %%b --ddu %%a --dry-run
    if not errorlevel 1 (echo "-------------- Dry-run of /datacopy_pyhub delete -m customer --code %%b --ddu %%a/ is COMPLETED with Exit Code=!ERRORLEVEL!") else (echo "ERROR -------------- Dry-run of /datacopy_pyhub delete -m customer --code %%b --ddu %%a/ is FAILED with Exit Code=!ERRORLEVEL!" 
    EXIT /B !ERRORLEVEL! 
    ) 
  )
)

:export_gpg_keys_after_deletion_dry_run
echo "---------- Export of gpg keys after deletion dry-run is starting"
mkdir %GPG_ETALON_DATA_DIR%\AfterDeletionDryRun

for %%x in (%GPG_KEY_ARRAY%) do (
  for /f "tokens=1-2 delims=," %%a in (%%x) do (
    echo  
	gpg --export %%b > %GPG_ETALON_DATA_DIR%\AfterDeletionDryRun\%%a
  )
)

for %%x in (%GPG_SECRET_KEY_ARRAY%) do (
  for /f "tokens=1-2 delims=," %%a in (%%x) do (
    echo  
	gpg --export-secret-keys %%b > %GPG_ETALON_DATA_DIR%\AfterDeletionDryRun\%%a
  )
)

echo "---------- Export of gpg keys after deletion dry-run is completed"

:export_mongo_db_collections_after_customer_deletion_dry_run
echo "---------- Export of mongo db collections after deletion dry-run is starting"

rmdir /s /q %JSON_ETALON_DATA_DIR%\AfterDeletionDryRun

mongoexport --host %MongoDB__HOST_IP% --port %MongoDB__PORT% -u %MongoDB__USER% -p %MongoDB__PASSWORD% --authenticationDatabase=%MongoDB__AUTH_DATABASE% --authenticationMechanism=%MongoDB__AUTH_MECHANISM% --db %MongoDB__DATABASE% --collection foCounters --query "{customer: null}" --out %JSON_ETALON_DATA_DIR%\AfterDeletionDryRun\null\foCounters_null.json --jsonArray

for %%x in (foCounters foCounters.HSBC) do (
for %%y in (KITS AST BERMUDA INTEGRATION FAKITS FASMOKE CGA01 ) do (
mongoexport --host %MongoDB__HOST_IP% --port %MongoDB__PORT% -u %MongoDB__USER% -p %MongoDB__PASSWORD% --authenticationDatabase=%MongoDB__AUTH_DATABASE% --authenticationMechanism=%MongoDB__AUTH_MECHANISM% --db %MongoDB__DATABASE% --collection %%x --query "{customer: '%%y'}" --out %JSON_ETALON_DATA_DIR%\AfterDeletionDryRun\%%y\%%x_%%y.json --jsonArray
)
)
echo "---------- Export of mongo db collections after deletion dry-run is completed"

:validation_of_deletion_dry_run
rmdir /s /q %TEMP1%\
rmdir /s /q %TEMP2%\
mkdir %TEMP1%
mkdir %TEMP2%

xcopy /s /e /y /f %ETALON_BEFORE_DATACOPY%\keys %TEMP1%\keys\
xcopy /s /e /y /f %ETALON_BEFORE_DATACOPY%\Tables %TEMP1%\Tables\
xcopy /s /e /y /f %ETALON_BEFORE_DATACOPY%\bkp_split %TEMP1%\bkp_split\
xcopy /s /e /y /f %ETALON_BEFORE_DATACOPY%\DB %TEMP1%\DB\
xcopy /s /e /y /f %ETALON_BEFORE_DATACOPY%\kStaIn %TEMP1%\Kanban\kStaIn\

xcopy /s /e /y /f %DATA_PATH%\keys %TEMP2%\keys\
xcopy /s /e /y /f %DATA_PATH%\Tables %TEMP2%\Tables\
xcopy /s /e /y /f %DATA_PATH%\bkp_split %TEMP2%\bkp_split\
xcopy /s /e /y /f %DATA_PATH%\DB %TEMP2%\DB\
xcopy /s /e /y /f %DATA_PATH%\Kanban\kStaIn %TEMP2%\Kanban\kStaIn\


cd /D %TEMP1%
for /r %%d in (*) do copy "%%d" "%TEMP1%"

cd /D %TEMP2%
for /r %%d in (*) do copy "%%d" "%TEMP2%"

cd /D %TEMP1%

FOR %%i IN (*) DO (
echo %%i


echo n|comp "%TEMP1%\%%i" "%TEMP2%\%%i" 
if not errorlevel 1 (echo "%%i is OK") else (echo "ERROR -------------- Comparison after deletion dry-run FAILED for %%i with Exit Code=!ERRORLEVEL!" 
EXIT /B !ERRORLEVEL! 
) 
)

echo "-------------- Comparison of JSON after deletion dry-run is STARTING" 


echo n|comp "%JSON_ETALON_DATA_DIR%\beforeExport\null\foCounters_null.json" "%JSON_ETALON_DATA_DIR%\AfterDeletionDryRun\null\foCounters_null.json"
if not errorlevel 1 (echo "foCounters_null.json is OK") else (echo "ERROR -------------- Comparison after deletion dry-run FAILED for foCounters_null.json with Exit Code=!ERRORLEVEL!" 
EXIT /B !ERRORLEVEL! 
) 
)

for %%x in (foCounters foCounters.HSBC) do (
for %%y in (KITS AST BERMUDA INTEGRATION FAKITS FASMOKE CGA01) do (
echo n|comp "%JSON_ETALON_DATA_DIR%\beforeExport\%%y\%%x_%%y.json" "%JSON_ETALON_DATA_DIR%\AfterDeletionDryRun\%%y\%%x_%%y.json"
if not errorlevel 1 (echo "%%x_%%y.json is OK") else (echo "ERROR -------------- Comparison after deletion dry-run FAILED for %%x_%%y.json with Exit Code=!ERRORLEVEL!" 
EXIT /B !ERRORLEVEL! 
) 
)
)
)

echo "-------------- Comparison of JSON after deletion dry-run is COMPLETED" 

echo "-------------- Comparison of GPG after deletion dry-run is STARTING" 

for %%x in (%GPG_KEY_ARRAY%) do (
  for /f "tokens=1-2 delims=," %%a in (%%x) do (
    echo n|comp "%ETALON_BEFORE_DATACOPY_GPG%\%%a" "%GPG_ETALON_DATA_DIR%\AfterDeletionDryRun\%%a"
    if not errorlevel 1 (echo "%%a is OK") else (echo "ERROR -------------- Comparison after deletion dry-run FAILED for %%a with Exit Code=!ERRORLEVEL!" 
    EXIT /B !ERRORLEVEL! 
    )
)
)

for %%x in (%GPG_SECRET_KEY_ARRAY%) do (
  for /f "tokens=1-2 delims=," %%a in (%%x) do (
    echo n|comp "%ETALON_BEFORE_DATACOPY_GPG%\%%a" "%GPG_ETALON_DATA_DIR%\AfterDeletionDryRun\%%a"
    if not errorlevel 1 (echo "%%a is OK") else (echo "ERROR -------------- Comparison after deletion dry-run FAILED for %%a with Exit Code=!ERRORLEVEL!" 
    EXIT /B !ERRORLEVEL! 
    )
)
)

echo "-------------- Comparison of GPG after deletion dry-run is COMPLETED" 



:delete_customer
cd /D %BIN_DIRECTORY%

for %%x in (%CUSTOMER_ARRAY%) do (
  for /f "tokens=1-2 delims=," %%a in (%%x) do (
    echo %%b %%a 
	echo "-------------- Dry-run of /datacopy_pyhub delete -m customer --code %%b --ddu %%a/ is STARTING"
    cmd /c datacopy_pyhub.cmd delete -m customer --code %%b --ddu %%a
	if not errorlevel 1 (echo "-------------- Deletion of customer data /datacopy_pyhub delete -m customer --code %%b --ddu %%a/ is COMPLETED with Exit Code=!ERRORLEVEL!") else (echo "ERROR -------------- Deletion of customer data /datacopy_pyhub delete -m customer --code %%b --ddu %%a/ is FAILED with Exit Code=!ERRORLEVEL!" 
    EXIT /B !ERRORLEVEL! 
    )
  )
)

:export_gpg_keys_after customer_deletion
echo "---------- Export of gpg keys after customer deletion is starting"
mkdir %GPG_ETALON_DATA_DIR%\AfterDeletion

for %%x in (%GPG_KEY_ARRAY%) do (
  for /f "tokens=1-2 delims=," %%a in (%%x) do (
    echo  
	gpg --export %%b > %GPG_ETALON_DATA_DIR%\AfterDeletion\%%a
  )
)

for %%x in (%GPG_SECRET_KEY_ARRAY%) do (
  for /f "tokens=1-2 delims=," %%a in (%%x) do (
    echo  
	gpg --export-secret-keys %%b > %GPG_ETALON_DATA_DIR%\AfterDeletion\%%a
  )
)

echo "---------- Export of gpg keys after customer deletion is completed"


:export_mongo_db_collections_after_customer_deletion
echo "---------- Export of mongo db collections after deletion is starting"

rmdir /s /q %JSON_ETALON_DATA_DIR%\AfterDeletion

mongoexport --host %MongoDB__HOST_IP% --port %MongoDB__PORT% -u %MongoDB__USER% -p %MongoDB__PASSWORD% --authenticationDatabase=%MongoDB__AUTH_DATABASE% --authenticationMechanism=%MongoDB__AUTH_MECHANISM% --db %MongoDB__DATABASE% --collection foCounters --query "{customer: null}" --out %JSON_ETALON_DATA_DIR%\AfterDeletion\null\foCounters_null.json --jsonArray

for %%x in (foCounters foCounters.HSBC) do (
for %%y in (KITS AST BERMUDA INTEGRATION FAKITS FASMOKE CGA01 ) do (
mongoexport --host %MongoDB__HOST_IP% --port %MongoDB__PORT% -u %MongoDB__USER% -p %MongoDB__PASSWORD% --authenticationDatabase=%MongoDB__AUTH_DATABASE% --authenticationMechanism=%MongoDB__AUTH_MECHANISM% --db %MongoDB__DATABASE% --collection %%x --query "{customer: '%%y'}" --out %JSON_ETALON_DATA_DIR%\AfterDeletion\%%y\%%x_%%y.json --jsonArray
)
)
echo "---------- Export of mongo db collections after deletion is completed"

:validation_of_customer_deletion
echo "-------------- Validation of customer deletion is STARTING"
rmdir /s /q %TEMP1%\
rmdir /s /q %TEMP2%\
mkdir %TEMP1%
mkdir %TEMP2%

xcopy /s /e /y /f %ETALON_AFTER_CUSTOMER_DELETION%\keys %TEMP1%\keys\
xcopy /s /e /y /f %ETALON_AFTER_CUSTOMER_DELETION%\Tables %TEMP1%\Tables\
xcopy /s /e /y /f %ETALON_AFTER_CUSTOMER_DELETION%\bkp_split %TEMP1%\bkp_split\
xcopy /s /e /y /f %ETALON_AFTER_CUSTOMER_DELETION%\kStaIn %TEMP1%\kStaIn\

xcopy /s /e /y /f %DATA_PATH%\keys %TEMP2%\keys\
xcopy /s /e /y /f %DATA_PATH%\Tables %TEMP2%\Tables\
xcopy /s /e /y /f %DATA_PATH%\bkp_split %TEMP2%\bkp_split\
xcopy /s /e /y /f %DATA_PATH%\Kanban\kStaIn %TEMP2%\kStaIn\


cd /D %TEMP1%
for /r %%d in (*) do copy "%%d" "%TEMP1%"

cd /D %TEMP2%
for /r %%d in (*) do copy "%%d" "%TEMP2%"

cd /D %TEMP1%

FOR %%i IN (*) DO (
echo %%i

echo n|comp "%TEMP2%\%%i" "%TEMP1%\%%i" >> d:\comparison_after_deletion.txt
if not errorlevel 1 (echo "-------------- Comparison after deletion passed for %%i with Exit Code=!ERRORLEVEL!") else (echo "ERROR -------------- Comparison after deletion FAILED for %%i with Exit Code=!ERRORLEVEL!" 
EXIT /B !ERRORLEVEL! 
)
)

cd /D %ETALON_AFTER_CUSTOMER_DELETION%\DB

FOR %%i IN (*) DO (
echo "--------------  Comparison after customer deletion is STARTING for %%i"

python %SCRIPTS_DIR%\compare_shelf.py %ETALON_AFTER_CUSTOMER_DELETION%\DB\%%i %DATA_PATH%\DB\%%i
if not errorlevel 1 (echo "-------------- Comparison after deletion passed for %%i" with Exit Code=!ERRORLEVEL!) else (echo "ERROR -------------- Comparison after deletion FAILED for %%i with Exit Code=!ERRORLEVEL!" 
EXIT /B !ERRORLEVEL!
) 
)

echo "-------------- Comparison of JSON after customer deletion is STARTING" 

echo n|comp "%JSON_ETALON_DATA_DIR%\beforeExport\null\foCounters_null.json" "%JSON_ETALON_DATA_DIR%\AfterDeletion\null\foCounters_null.json"
if not errorlevel 1 (echo "foCounters_null.json is OK") else (echo "ERROR -------------- Comparison after customer deletion FAILED for foCounters_null.json with Exit Code=!ERRORLEVEL!" 
EXIT /B !ERRORLEVEL! 
) 
)

for %%x in (foCounters foCounters.HSBC) do (
for %%y in (AST BERMUDA INTEGRATION FAKITS FASMOKE CGA01) do (
echo n|comp "%JSON_ETALON_DATA_DIR%\beforeExport\%%y\%%x_%%y.json" "%JSON_ETALON_DATA_DIR%\AfterDeletion\%%y\%%x_%%y.json"
if not errorlevel 1 (echo "%%x_%%y.json is OK") else (echo "ERROR -------------- Comparison after customer deletion FAILED for %%x_%%y.json with Exit Code=!ERRORLEVEL!" 
EXIT /B !ERRORLEVEL! 
) 
)
)
)

for %%x in (foCounters foCounters.HSBC) do (
for %%y in (KITS) do (
echo n|comp "%JSON_ETALON_DATA_DIR%\empty\%%y\%%x_%%y.json" "%JSON_ETALON_DATA_DIR%\AfterDeletion\%%y\%%x_%%y.json"
if not errorlevel 1 (echo "%%x_%%y.json is OK") else (echo "ERROR -------------- Comparison after customer deletion FAILED for %%x_%%y.json with Exit Code=!ERRORLEVEL!" 
EXIT /B !ERRORLEVEL! 
) 
)
)
)

echo "-------------- Comparison of JSON after customer deletion is COMPLETED" 


echo "-------------- Comparison of GPG after customer deletion is STARTING" 

for %%x in (%GPG_KEY_ARRAY%) do (
  for /f "tokens=1-2 delims=," %%a in (%%x) do (
    echo n|comp "%GPG_ETALON_DATA_DIR%\gpg_keys_original\%%a" "%GPG_ETALON_DATA_DIR%\AfterDeletion\%%a"
    if not errorlevel 1 (echo "%%a is OK") else (echo "ERROR -------------- Comparison after customer deletion FAILED for %%a with Exit Code=!ERRORLEVEL!" 
    EXIT /B !ERRORLEVEL! 
    )
)
)

for %%x in (%GPG_SECRET_KEY_ARRAY%) do (
  for /f "tokens=1-2 delims=," %%a in (%%x) do (
    echo n|comp "%GPG_ETALON_DATA_DIR%\gpg_keys_original\%%a" "%GPG_ETALON_DATA_DIR%\AfterDeletion\%%a"
    if not errorlevel 1 (echo "%%a is OK") else (echo "ERROR -------------- Comparison after customer deletion FAILED for %%a with Exit Code=!ERRORLEVEL!" 
    EXIT /B !ERRORLEVEL! 
    )
)
)

echo "-------------- Comparison of GPG after customer deletion is COMPLETED" 

echo "-------------- Validation of customer deletion is COMPLETED



:to_be_removed_when_PYH-743_is_fixed
echo "---------- FORCED DELETION OF KITS GPG KEYS is STARTING"
echo "---------- To be deleted after PYH-743 is fixed."
gpg --batch --fingerprint --yes --delete-secret-key 2A68D9340F6277BAB484A8224FB7368E16260693
gpg --batch --fingerprint --yes --delete-secret-key AA56E7DDE91D3A080F9F5B81F61C2C548BBADC46
gpg --batch --fingerprint --yes --delete-secret-key CE6A9BC8669CCFFEE06473836DA5620B5E1A1456
gpg --batch --yes --delete-key 2A68D9340F6277BAB484A8224FB7368E16260693
gpg --batch --yes --delete-key AA56E7DDE91D3A080F9F5B81F61C2C548BBADC46
gpg --batch --yes --delete-key CE6A9BC8669CCFFEE06473836DA5620B5E1A1456
echo "---------- FORCED DELETION OF KITS GPG KEYS  is COMPLETED



:disable_constraints_in_oracle_db
echo "-------------- Disabling constraints in Oracle DB is STARTING"
sqlplus %DB_CONNECTION_STRING% @%SCRIPTS_DIR%\disable_constraints.sql
echo "-------------- Disabling constraints in Oracle DB is COMPLETED"


:change_customer_codes_in_oracle_db
echo "-------------- Changing customer codes in Oracle DB is STARTING"
del /q %SCRIPTS_DIR%\change_customer_codes_in_oracle_db.sql
echo SET ECHO ON; > %SCRIPTS_DIR%\change_customer_codes_in_oracle_db.sql

for %%x in (%CUSTOMER_ARRAY%) do (
  for /f "tokens=1-2 delims=," %%a in (%%x) do (
    echo UPDATE %TARGET_SCHEMA%.CORPORATE_CUSTOMER SET CUSTOMER_ID = '%RANDOM%' WHERE INTERFACE_CODE = '%%b'; >> %SCRIPTS_DIR%\change_customer_codes_in_oracle_db.sql
    echo UPDATE %TARGET_SCHEMA%.CORPORATE_CUSTOMER SET CORP_CUSTOMER_ID = 'REMOVED_%%b' WHERE INTERFACE_CODE = '%%b'; >> %SCRIPTS_DIR%\change_customer_codes_in_oracle_db.sql
	echo UPDATE %TARGET_SCHEMA%.CORPORATE_CUSTOMER SET INTERFACE_CODE = 'REMOVED_%%b' WHERE INTERFACE_CODE = '%%b'; >> %SCRIPTS_DIR%\change_customer_codes_in_oracle_db.sql
    echo SELECT INTERFACE_CODE, CORP_CUSTOMER_ID, IS_ACTIVE, CUSTOMER_ID from %TARGET_SCHEMA%.CORPORATE_CUSTOMER WHERE INTERFACE_CODE = 'REMOVED_%%b'; >> %SCRIPTS_DIR%\change_customer_codes_in_oracle_db.sql
  )
)

echo EXIT >> %SCRIPTS_DIR%\change_customer_codes_in_oracle_db.sql
sqlplus %DB_CONNECTION_STRING% @%SCRIPTS_DIR%\change_customer_codes_in_oracle_db.sql
echo "-------------- Changing customer codes in Oracle DB is COMPLETED"


:import_customer_data_dry_run
cd /D %BIN_DIRECTORY%

for %%x in (%CUSTOMER_ARRAY%) do (
  for /f "tokens=1-2 delims=," %%a in (%%x) do (
    echo %%b %%a 
	echo "-------------- Dry-run of /datacopy_pyhub import -m customer --code %%b --ddu %%a/ is STARTING"
    cmd /c datacopy_pyhub.cmd import -m customer --code %%b --ddu %%a --dry-run
	if not errorlevel 1 (echo "-------------- Dry-run of /datacopy_pyhub import -m customer --code %%b --ddu %%a/ is COMPLETED with Exit Code=!ERRORLEVEL!") else (echo "ERROR -------------- Dry-run of /datacopy_pyhub import -m customer --code %%b --ddu %%a/ is FAILED with Exit Code=!ERRORLEVEL!" 
    EXIT /B !ERRORLEVEL! 
    ) 
  )
)



:export_gpg_keys_after import_dry_run
echo "---------- Export of gpg keys after import dry-run is starting"
mkdir %GPG_ETALON_DATA_DIR%\AfterImportDryRun

for %%x in (%GPG_KEY_ARRAY%) do (
  for /f "tokens=1-2 delims=," %%a in (%%x) do (
    echo  
	gpg --export %%b > %GPG_ETALON_DATA_DIR%\AfterImportDryRun\%%a
  )
)

for %%x in (%GPG_SECRET_KEY_ARRAY%) do (
  for /f "tokens=1-2 delims=," %%a in (%%x) do (
    echo  
	gpg --export-secret-keys %%b > %GPG_ETALON_DATA_DIR%\AfterImportDryRun\%%a
  )
)

echo "---------- Export of gpg keys after import dry-run is completed"


:export_mongo_db_collections_after_import_dry_run
echo "---------- Export of mongo db collections after import dry-run is starting"

rmdir /s /q %JSON_ETALON_DATA_DIR%\AfterImportDryRun

mongoexport --host %MongoDB__HOST_IP% --port %MongoDB__PORT% -u %MongoDB__USER% -p %MongoDB__PASSWORD% --authenticationDatabase=%MongoDB__AUTH_DATABASE% --authenticationMechanism=%MongoDB__AUTH_MECHANISM% --db %MongoDB__DATABASE% --collection foCounters --query "{customer: null}" --out %JSON_ETALON_DATA_DIR%\AfterImportDryRun\null\foCounters_null.json --jsonArray

for %%x in (foCounters foCounters.HSBC) do (
for %%y in (KITS AST BERMUDA INTEGRATION FAKITS FASMOKE CGA01 ) do (
mongoexport --host %MongoDB__HOST_IP% --port %MongoDB__PORT% -u %MongoDB__USER% -p %MongoDB__PASSWORD% --authenticationDatabase=%MongoDB__AUTH_DATABASE% --authenticationMechanism=%MongoDB__AUTH_MECHANISM% --db %MongoDB__DATABASE% --collection %%x --query "{customer: '%%y'}" --out %JSON_ETALON_DATA_DIR%\AfterImportDryRun\%%y\%%x_%%y.json --jsonArray
)
)
echo "---------- Export of mongo db collections after import dry-run is completed"


:validation_of_import_dry_run
echo "-------------- Validation of import dry-run is STARTING"
rmdir /s /q %TEMP1%\
rmdir /s /q %TEMP2%\
mkdir %TEMP1%
mkdir %TEMP2%

xcopy /s /e /y /f %ETALON_AFTER_CUSTOMER_DELETION%\keys %TEMP1%\keys\
xcopy /s /e /y /f %ETALON_AFTER_CUSTOMER_DELETION%\Tables %TEMP1%\Tables\
xcopy /s /e /y /f %ETALON_AFTER_CUSTOMER_DELETION%\bkp_split %TEMP1%\bkp_split\
xcopy /s /e /y /f %ETALON_AFTER_CUSTOMER_DELETION%\kStaIn %TEMP1%\kStaIn\

xcopy /s /e /y /f %DATA_PATH%\keys %TEMP2%\keys\
xcopy /s /e /y /f %DATA_PATH%\Tables %TEMP2%\Tables\
xcopy /s /e /y /f %DATA_PATH%\bkp_split %TEMP2%\bkp_split\
xcopy /s /e /y /f %DATA_PATH%\Kanban\kStaIn %TEMP2%\kStaIn\


cd /D %TEMP1%
for /r %%d in (*) do copy "%%d" "%TEMP1%"

cd /D %TEMP2%
for /r %%d in (*) do copy "%%d" "%TEMP2%"

cd /D %TEMP1%

FOR %%i IN (*) DO (
echo %%i

echo n|comp "%TEMP1%\%%i" "%TEMP2%\%%i"
if not errorlevel 1 (echo "-------------- Comparison after import dry-run passed for %%i with Exit Code=!ERRORLEVEL!") else (echo "ERROR -------------- Comparison after import dry-run FAILED for %%i with Exit Code=!ERRORLEVEL!" 
EXIT /B !ERRORLEVEL! 
)
)

cd /D %ETALON_AFTER_CUSTOMER_DELETION%\DB

FOR %%i IN (*) DO (
echo "--------------  Comparison after import dry-run is STARTING for %%i"

python %SCRIPTS_DIR%\compare_shelf.py %ETALON_AFTER_CUSTOMER_DELETION%\DB\%%i %DATA_PATH%\DB\%%i
if not errorlevel 1 (echo "-------------- Comparison after import dry-run passed for %%i" with Exit Code=!ERRORLEVEL!) else (echo "ERROR -------------- Comparison after import dry-run FAILED for %%i with Exit Code=!ERRORLEVEL!" 
EXIT /B !ERRORLEVEL!
) 
)

echo "-------------- Comparison of JSON after import dry-run is STARTING" 

echo n|comp "%JSON_ETALON_DATA_DIR%\beforeExport\null\foCounters_null.json" "%JSON_ETALON_DATA_DIR%\AfterImportDryRun\null\foCounters_null.json"
if not errorlevel 1 (echo "foCounters_null.json is OK") else (echo "ERROR -------------- Comparison after import dry-run FAILED for foCounters_null.json with Exit Code=!ERRORLEVEL!" 
EXIT /B !ERRORLEVEL! 
) 
)

for %%x in (foCounters foCounters.HSBC) do (
for %%y in (AST BERMUDA INTEGRATION FAKITS FASMOKE CGA01) do (
echo n|comp "%JSON_ETALON_DATA_DIR%\beforeExport\%%y\%%x_%%y.json" "%JSON_ETALON_DATA_DIR%\AfterImportDryRun\%%y\%%x_%%y.json"
if not errorlevel 1 (echo "%%x_%%y.json is OK") else (echo "ERROR -------------- Comparison after import dry-run FAILED for %%x_%%y.json with Exit Code=!ERRORLEVEL!" 
EXIT /B !ERRORLEVEL! 
) 
)
)
)

for %%x in (foCounters foCounters.HSBC) do (
for %%y in (KITS) do (
echo n|comp "%JSON_ETALON_DATA_DIR%\empty\%%y\%%x_%%y.json" "%JSON_ETALON_DATA_DIR%\AfterImportDryRun\%%y\%%x_%%y.json"
if not errorlevel 1 (echo "%%x_%%y.json is OK") else (echo "ERROR -------------- Comparison after import dry-run FAILED for %%x_%%y.json with Exit Code=!ERRORLEVEL!" 
EXIT /B !ERRORLEVEL! 
) 
)
)
)

echo "-------------- Comparison of JSON after import dry-run is COMPLETED" 

echo "-------------- Comparison of GPG after import dry-run is STARTING" 

for %%x in (%GPG_KEY_ARRAY%) do (
  for /f "tokens=1-2 delims=," %%a in (%%x) do (
    echo n|comp "%GPG_ETALON_DATA_DIR%\AfterDeletionEtalon\%%a" "%GPG_ETALON_DATA_DIR%\AfterImportDryRun\%%a"
    if not errorlevel 1 (echo "%%a is OK") else (echo "ERROR -------------- Comparison after import dry-run FAILED for %%a with Exit Code=!ERRORLEVEL!" 
    EXIT /B !ERRORLEVEL! 
    )
)
)

for %%x in (%GPG_SECRET_KEY_ARRAY%) do (
  for /f "tokens=1-2 delims=," %%a in (%%x) do (
    echo n|comp "%GPG_ETALON_DATA_DIR%\AfterDeletionEtalon\%%a" "%GPG_ETALON_DATA_DIR%\AfterImportDryRun\%%a"
    if not errorlevel 1 (echo "%%a is OK") else (echo "ERROR -------------- Comparison after import dry-run FAILED for %%a with Exit Code=!ERRORLEVEL!" 
    EXIT /B !ERRORLEVEL! 
    )
)
)

echo "-------------- Comparison of GPG after import dry-run is COMPLETED" 

echo "-------------- Validation of import dry-run is COMPLETED"



:import_customer_data
cd /D %BIN_DIRECTORY%

for %%x in (%CUSTOMER_ARRAY%) do (
  for /f "tokens=1-2 delims=," %%a in (%%x) do (
    echo %%b %%a 
	echo "-------------- Import of customer data /datacopy_pyhub import -m customer --code %%b --ddu %%a/ is STARTING"
    cmd /c datacopy_pyhub.cmd import -m customer --code %%b --ddu %%a
	if not errorlevel 1 (echo "-------------- Import of customer data /datacopy_pyhub import -m customer --code %%b --ddu %%a/ is COMPLETED with Exit Code=!ERRORLEVEL!") else (echo "ERROR -------------- Import of customer data /datacopy_pyhub import -m customer --code %%b --ddu %%a/ is FAILED with Exit Code=!ERRORLEVEL!" 
    EXIT /B !ERRORLEVEL! 
    ) 
  )
)


:export_gpg_keys_after_customer import
echo "---------- Export of gpg keys after customer import is starting"
mkdir %GPG_ETALON_DATA_DIR%\AfterImport

for %%x in (%GPG_KEY_ARRAY%) do (
  for /f "tokens=1-2 delims=," %%a in (%%x) do (
    echo  
	gpg --export %%b > %GPG_ETALON_DATA_DIR%\AfterImport\%%a
  )
)

for %%x in (%GPG_SECRET_KEY_ARRAY%) do (
  for /f "tokens=1-2 delims=," %%a in (%%x) do (
    echo  
	gpg --export-secret-keys %%b > %GPG_ETALON_DATA_DIR%\AfterImport\%%a
  )
)

echo "---------- Export of gpg keys after customer import is completed"


:export_mongo_db_collections_after_customer_import
echo "---------- Export of mongo db collections after customer import is starting"

rmdir /s /q %JSON_ETALON_DATA_DIR%\AfterImport

mongoexport --host %MongoDB__HOST_IP% --port %MongoDB__PORT% -u %MongoDB__USER% -p %MongoDB__PASSWORD% --authenticationDatabase=%MongoDB__AUTH_DATABASE% --authenticationMechanism=%MongoDB__AUTH_MECHANISM% --db %MongoDB__DATABASE% --collection foCounters --query "{customer: null}" --out %JSON_ETALON_DATA_DIR%\AfterImport\null\foCounters_null.json --jsonArray

for %%x in (foCounters foCounters.HSBC) do (
for %%y in (KITS AST BERMUDA INTEGRATION FAKITS FASMOKE CGA01 ) do (
mongoexport --host %MongoDB__HOST_IP% --port %MongoDB__PORT% -u %MongoDB__USER% -p %MongoDB__PASSWORD% --authenticationDatabase=%MongoDB__AUTH_DATABASE% --authenticationMechanism=%MongoDB__AUTH_MECHANISM% --db %MongoDB__DATABASE% --collection %%x --query "{customer: '%%y'}" --out %JSON_ETALON_DATA_DIR%\AfterImport\%%y\%%x_%%y.json --jsonArray
)
)
echo "---------- Export of mongo db collections after customer import is completed"

:validation_of_customer_import
echo "-------------- Validation of customer import is STARTING"
rmdir /s /q %TEMP1%\
rmdir /s /q %TEMP2%\
mkdir %TEMP1%
mkdir %TEMP2%

xcopy /s /e /y /f %ETALON_AFTER_CUSTOMER_IMPORT%\keys %TEMP1%\keys\
xcopy /s /e /y /f %ETALON_AFTER_CUSTOMER_IMPORT%\Tables %TEMP1%\Tables\
xcopy /s /e /y /f %ETALON_AFTER_CUSTOMER_IMPORT%\bkp_split %TEMP1%\bkp_split\
xcopy /s /e /y /f %ETALON_AFTER_CUSTOMER_IMPORT%\kStaIn %TEMP1%\kStaIn\

xcopy /s /e /y /f %DATA_PATH%\keys %TEMP2%\keys\
xcopy /s /e /y /f %DATA_PATH%\Tables %TEMP2%\Tables\
xcopy /s /e /y /f %DATA_PATH%\bkp_split %TEMP2%\bkp_split\
xcopy /s /e /y /f %DATA_PATH%\Kanban\kStaIn %TEMP2%\kStaIn\


cd /D %TEMP1%
for /r %%d in (*) do copy "%%d" "%TEMP1%"

cd /D %TEMP2%
for /r %%d in (*) do copy "%%d" "%TEMP2%"

cd /D %TEMP1%

FOR %%i IN (*) DO (
echo %%i

echo n|comp "%TEMP1%\%%i" "%TEMP2%\%%i" >> d:\comparison_after_import.txt
if not errorlevel 1 (echo "-------------- Comparison after customer import passed for %%i with Exit Code=!ERRORLEVEL!") else (echo "ERROR -------------- Comparison after customer import FAILED for %%i with Exit Code=!ERRORLEVEL!" 
EXIT /B !ERRORLEVEL! 
)
)

cd /D %ETALON_AFTER_CUSTOMER_IMPORT%\DB

FOR %%i IN (*) DO (
echo "--------------  Comparison after customer import is STARTING for %%i"

python %SCRIPTS_DIR%\compare_shelf.py %ETALON_AFTER_CUSTOMER_IMPORT%\DB\%%i %DATA_PATH%\DB\%%i
if not errorlevel 1 (echo "-------------- Comparison after customer import passed for %%i" with Exit Code=!ERRORLEVEL!) else (echo "ERROR -------------- Comparison after customer import FAILED for %%i with Exit Code=!ERRORLEVEL!" 
EXIT /B !ERRORLEVEL!
) 
)

echo "-------------- Comparison of JSON after customer import is STARTING" 

cd /D %TEST_HOME_DIR%

python %SCRIPTS_DIR%\Compare_JSON.py %JSON_ETALON_DATA_DIR%\AfterImport_etalon\null\foCounters_null.json %JSON_ETALON_DATA_DIR%\AfterImport\null\foCounters_null.json
if not errorlevel 1 (echo "foCounters_null.json is OK") else (echo "ERROR -------------- Comparison after customer import FAILED for foCounters_null.json with Exit Code=!ERRORLEVEL!" 
EXIT /B !ERRORLEVEL! 
) 
)

for %%x in (foCounters foCounters.HSBC) do (
for %%y in (AST BERMUDA INTEGRATION FAKITS FASMOKE CGA01) do (
echo n|comp "%JSON_ETALON_DATA_DIR%\beforeExport\%%y\%%x_%%y.json" "%JSON_ETALON_DATA_DIR%\AfterImport\%%y\%%x_%%y.json"
if not errorlevel 1 (echo "%%x_%%y.json is OK") else (echo "ERROR -------------- Comparison after customer import FAILED for %%x_%%y.json with Exit Code=!ERRORLEVEL!" 
EXIT /B !ERRORLEVEL! 
) 
)
)
)

for %%x in (foCounters foCounters.HSBC) do (
for %%y in (KITS) do (
python %SCRIPTS_DIR%\Compare_JSON.py %JSON_ETALON_DATA_DIR%\AfterImport_etalon\%%y\%%x_%%y.json %JSON_ETALON_DATA_DIR%\AfterImport\%%y\%%x_%%y.json
if not errorlevel 1 (echo "%%x_%%y.json is OK") else (echo "ERROR -------------- Comparison after customer import FAILED for %%x_%%y.json with Exit Code=!ERRORLEVEL!" 
EXIT /B !ERRORLEVEL! 
) 
)
)
)

echo "-------------- Comparison of JSON after customer import is COMPLETED" 


echo "-------------- Comparison of GPG after customer import is STARTING" 

REM for %%x in (%GPG_KEY_ARRAY%) do (
REM   for /f "tokens=1-2 delims=," %%a in (%%x) do (
REM     echo n|comp "%ETALON_BEFORE_DATACOPY_GPG%\%%a" "%GPG_ETALON_DATA_DIR%\AfterImport\%%a"
REM     if not errorlevel 1 (echo "%%a is OK") else (echo "ERROR -------------- Comparison after customer import FAILED for %%a with Exit Code=!ERRORLEVEL!" 
REM     EXIT /B !ERRORLEVEL! 
REM     )
REM )
REM )

REM for %%x in (%GPG_SECRET_KEY_ARRAY%) do (
REM   for /f "tokens=1-2 delims=," %%a in (%%x) do (
REM     echo n|comp "%ETALON_BEFORE_DATACOPY_GPG%\%%a" "%GPG_ETALON_DATA_DIR%\AfterImport\%%a"
REM     if not errorlevel 1 (echo "%%a is OK") else (echo "ERROR -------------- Comparison after customer import FAILED for %%a with Exit Code=!ERRORLEVEL!" 
REM     EXIT /B !ERRORLEVEL! 
REM     )
REM )
REM )

echo "-------------- Comparison of GPG after customer import is COMPLETED" 

echo "-------------- Validation of customer import is COMPLETED"


:restore_customer_codes_in_oracle_db
echo "-------------- Restoring customer codes in Oracle DB is STARTING"
del /q %SCRIPTS_DIR%\restore_customer_codes_in_oracle_db.sql

echo SET ECHO ON; > %SCRIPTS_DIR%\restore_customer_codes_in_oracle_db.sql

for %%x in (%CUSTOMER_ARRAY%) do (
  for /f "tokens=1-2 delims=," %%a in (%%x) do (
    echo UPDATE %TARGET_SCHEMA%.CORPORATE_CUSTOMER SET IS_ACTIVE = '1' WHERE INTERFACE_CODE = 'REMOVED_%%b'; >> %SCRIPTS_DIR%\restore_customer_codes_in_oracle_db.sql
    echo UPDATE %TARGET_SCHEMA%.CORPORATE_CUSTOMER SET CUSTOMER_ID = '%%a' WHERE INTERFACE_CODE = 'REMOVED_%%b'; >> %SCRIPTS_DIR%\restore_customer_codes_in_oracle_db.sql
    echo UPDATE %TARGET_SCHEMA%.CORPORATE_CUSTOMER SET CORP_CUSTOMER_ID = '%%b' WHERE INTERFACE_CODE = 'REMOVED_%%b'; >> %SCRIPTS_DIR%\restore_customer_codes_in_oracle_db.sql
    echo UPDATE %TARGET_SCHEMA%.CORPORATE_CUSTOMER SET INTERFACE_CODE = '%%b' WHERE INTERFACE_CODE = 'REMOVED_%%b'; >> %SCRIPTS_DIR%\restore_customer_codes_in_oracle_db.sql
    echo SELECT INTERFACE_CODE, CORP_CUSTOMER_ID, IS_ACTIVE, CUSTOMER_ID from %TARGET_SCHEMA%.CORPORATE_CUSTOMER WHERE INTERFACE_CODE = '%%b'; >> %SCRIPTS_DIR%\restore_customer_codes_in_oracle_db.sql
  )
)

echo EXIT >> %SCRIPTS_DIR%\restore_customer_codes_in_oracle_db.sql
sqlplus %DB_CONNECTION_STRING% @%SCRIPTS_DIR%\restore_customer_codes_in_oracle_db.sql
echo "-------------- Changing customer codes in Oracle DB is COMPLETED"


:inactivate_customer
echo "-------------- Inactivation of customer is STARTING"
sqlplus %DB_CONNECTION_STRING% @%SCRIPTS_DIR%\inactivate_customer.sql
echo "-------------- Inactivation of customer is COMPLETED"


:delete_customer_scenario2
cd /D %BIN_DIRECTORY%

for %%x in (%CUSTOMER_ARRAY%) do (
  for /f "tokens=1-2 delims=," %%a in (%%x) do (
    echo %%b %%a 
    echo "-------------- Deletion of customer data - SCENARIO 2 /datacopy_pyhub delete -m customer --code %%b --ddu %%a/ is STARTING"
    cmd /c datacopy_pyhub.cmd delete -m customer --code %%b --ddu %%a
    if not errorlevel 1 (echo "-------------- Deletion of customer data - SCENARIO 2 /datacopy_pyhub delete -m customer --code %%b --ddu %%a/ is COMPLETED with Exit Code=!ERRORLEVEL!") else (echo "ERROR -------------- Deletion of customer data - SCENARIO 2 /datacopy_pyhub import -m customer --code %%b --ddu %%a/ is FAILED with Exit Code=!ERRORLEVEL!" 
    EXIT /B !ERRORLEVEL!
    )
  )
)




:change_customer_codes_in_oracle_db
echo "-------------- Changing customer codes in Oracle DB is STARTING"
sqlplus %DB_CONNECTION_STRING% @%SCRIPTS_DIR%\change_customer_codes_in_oracle_db.sql
echo "-------------- Changing customer codes in Oracle DB is COMPLETED"


:import_customer_data_scenario2

rmdir /s /q %EXPORT_DATA_DIR%\pyhub

mkdir %EXPORT_DATA_DIR%\pyhub\KITS

xcopy /E %SCENARIO_2_DATA%\KITS %EXPORT_DATA_DIR%\pyhub\KITS 

cd /D %BIN_DIRECTORY%

for %%x in (%CUSTOMER_ARRAY%) do (
  for /f "tokens=1-2 delims=," %%a in (%%x) do (
    echo %%b %%a 
    echo "-------------- Import of customer data - SCENARIO 2 /datacopy_pyhub import -m customer --code %%b --ddu %%a/ is STARTING"
    cmd /c datacopy_pyhub.cmd import -m customer --code %%b --ddu %%a
    if not errorlevel 1 (echo "-------------- Import of customer data - SCENARIO 2 /datacopy_pyhub import -m customer --code %%b --ddu %%a/ is COMPLETED with Exit Code=!ERRORLEVEL!") else (echo "ERROR -------------- Import of customer data - SCENARIO 2 /datacopy_pyhub import -m customer --code %%b --ddu %%a/ is FAILED with Exit Code=!ERRORLEVEL!" 
    EXIT /B !ERRORLEVEL! 
    ) 
  )
)



:export_mongo_db_collections_after_customer_import_scenario2
echo "---------- Export of mongo db collections after customer import Scenario 2 is starting"

rmdir /s /q %JSON_ETALON_DATA_DIR%\AfterImportScenario2

mongoexport --host %MongoDB__HOST_IP% --port %MongoDB__PORT% -u %MongoDB__USER% -p %MongoDB__PASSWORD% --authenticationDatabase=%MongoDB__AUTH_DATABASE% --authenticationMechanism=%MongoDB__AUTH_MECHANISM% --db %MongoDB__DATABASE% --collection foCounters --query "{customer: null}" --out %JSON_ETALON_DATA_DIR%\AfterImportScenario2\null\foCounters_null.json --jsonArray

for %%x in (foCounters foCounters.HSBC) do (
for %%y in (KITS AST BERMUDA INTEGRATION FAKITS FASMOKE CGA01 ) do (
mongoexport --host %MongoDB__HOST_IP% --port %MongoDB__PORT% -u %MongoDB__USER% -p %MongoDB__PASSWORD% --authenticationDatabase=%MongoDB__AUTH_DATABASE% --authenticationMechanism=%MongoDB__AUTH_MECHANISM% --db %MongoDB__DATABASE% --collection %%x --query "{customer: '%%y'}" --out %JSON_ETALON_DATA_DIR%\AfterImportScenario2\%%y\%%x_%%y.json --jsonArray
)
)
echo "---------- Export of mongo db collections after customer import Scenario 2 is completed"

:validation_of_customer_import_scenario2
echo "-------------- Validation of customer import Scenario 2 is STARTING"
rmdir /s /q %TEMP1%\
rmdir /s /q %TEMP2%\
mkdir %TEMP1%
mkdir %TEMP2%

xcopy /s /e /y /f %ETALON_AFTER_CUSTOMER_IMPORT_SCENARIO_2%\Tables %TEMP1%\Tables\

xcopy /s /e /y /f %DATA_PATH%\Tables %TEMP2%\Tables\


cd /D %TEMP1%
for /r %%d in (*) do copy "%%d" "%TEMP1%"

cd /D %TEMP2%
for /r %%d in (*) do copy "%%d" "%TEMP2%"

cd /D %TEMP1%

FOR %%i IN (*) DO (
echo %%i

echo n|comp "%TEMP1%\%%i" "%TEMP2%\%%i" >> d:\comparison_after_import_scenario2.txt
if not errorlevel 1 (echo "-------------- Comparison after customer import Scenario 2 passed for %%i with Exit Code=!ERRORLEVEL!") else (echo "ERROR -------------- Comparison after customer import Scenario 2 FAILED for %%i with Exit Code=!ERRORLEVEL!" 
EXIT /B !ERRORLEVEL! 
)
)

cd /D %ETALON_AFTER_CUSTOMER_IMPORT_SCENARIO_2%\DB

FOR %%i IN (*) DO (
echo "--------------  Comparison after customer import Scenario 2 is STARTING for %%i"

python %SCRIPTS_DIR%\compare_shelf.py %ETALON_AFTER_CUSTOMER_IMPORT_SCENARIO_2%\DB\%%i %DATA_PATH%\DB\%%i
if not errorlevel 1 (echo "-------------- Comparison after customer import Scenario 2 passed for %%i" with Exit Code=!ERRORLEVEL!) else (echo "ERROR -------------- Comparison after customer import Scenario 2 FAILED for %%i with Exit Code=!ERRORLEVEL!" 
EXIT /B !ERRORLEVEL!
) 
)

echo "-------------- Comparison of JSON after customer import Scenario 2 is STARTING" 

cd /D %TEST_HOME_DIR%

python %SCRIPTS_DIR%\Compare_JSON.py %JSON_ETALON_DATA_DIR%\AfterImport_etalon_scenario2\null\foCounters_null.json %JSON_ETALON_DATA_DIR%\AfterImportScenario2\null\foCounters_null.json
if not errorlevel 1 (echo "foCounters_null.json is OK") else (echo "ERROR -------------- Comparison after customer import Scenario 2 FAILED for foCounters_null.json with Exit Code=!ERRORLEVEL!" 
EXIT /B !ERRORLEVEL! 
) 
)

for %%x in (foCounters foCounters.HSBC) do (
for %%y in (AST BERMUDA INTEGRATION FAKITS FASMOKE CGA01) do (
echo n|comp "%JSON_ETALON_DATA_DIR%\beforeExport\%%y\%%x_%%y.json" "%JSON_ETALON_DATA_DIR%\AfterImportScenario2\%%y\%%x_%%y.json"
if not errorlevel 1 (echo "%%x_%%y.json is OK") else (echo "ERROR -------------- Comparison after customer import Scenario 2 FAILED for %%x_%%y.json with Exit Code=!ERRORLEVEL!" 
EXIT /B !ERRORLEVEL! 
) 
)
)
)

for %%x in (foCounters foCounters.HSBC) do (
for %%y in (KITS) do (
python %SCRIPTS_DIR%\Compare_JSON.py %JSON_ETALON_DATA_DIR%\empty\%%y\%%x_%%y.json %JSON_ETALON_DATA_DIR%\AfterImportScenario2\%%y\%%x_%%y.json
if not errorlevel 1 (echo "%%x_%%y.json is OK") else (echo "ERROR -------------- Comparison after customer import Scenario 2 FAILED for %%x_%%y.json with Exit Code=!ERRORLEVEL!" 
EXIT /B !ERRORLEVEL! 
) 
)
)
)

echo "-------------- Comparison of JSON after customer import Scenario 2 is COMPLETED" 

echo "-------------- Validation of customer import Scenario 2 is COMPLETED"


:restore_customer_codes_in_oracle_db
echo "-------------- Restoring customer codes in Oracle DB is STARTING"
sqlplus %DB_CONNECTION_STRING% @%SCRIPTS_DIR%\restore_customer_codes_in_oracle_db.sql
echo "-------------- Changing customer codes in Oracle DB is COMPLETED"


:inactivate_customer
echo "-------------- Inactivation of customer is STARTING"
sqlplus %DB_CONNECTION_STRING% @%SCRIPTS_DIR%\inactivate_customer.sql
echo "-------------- Inactivation of customer is COMPLETED"


:delete_customer_scenario3
cd /D %BIN_DIRECTORY%

for %%x in (%CUSTOMER_ARRAY%) do (
  for /f "tokens=1-2 delims=," %%a in (%%x) do (
    echo %%b %%a 
    echo "-------------- Deletion of customer data - SCENARIO 3 /datacopy_pyhub delete -m customer --code %%b --ddu %%a/ is STARTING"
    cmd /c datacopy_pyhub.cmd delete -m customer --code %%b --ddu %%a
    if not errorlevel 1 (echo "-------------- Deletion of customer data - SCENARIO 3 /datacopy_pyhub delete -m customer --code %%b --ddu %%a/ is COMPLETED with Exit Code=!ERRORLEVEL!") else (echo "ERROR -------------- Deletion of customer data - SCENARIO 3 /datacopy_pyhub import -m customer --code %%b --ddu %%a/ is FAILED with Exit Code=!ERRORLEVEL!" 
    EXIT /B !ERRORLEVEL!
    )
  )
)


:change_customer_codes_in_oracle_db
echo "-------------- Changing customer codes in Oracle DB is STARTING"
sqlplus %DB_CONNECTION_STRING% @%SCRIPTS_DIR%\change_customer_codes_in_oracle_db.sql
echo "-------------- Changing customer codes in Oracle DB is COMPLETED"


:import_customer_data_scenario3

rmdir /s /q %EXPORT_DATA_DIR%\pyhub

mkdir %EXPORT_DATA_DIR%\pyhub\KITS

xcopy /E %SCENARIO_3_DATA%\KITS %EXPORT_DATA_DIR%\pyhub\KITS 

cd /D %BIN_DIRECTORY%

for %%x in (%CUSTOMER_ARRAY%) do (
  for /f "tokens=1-2 delims=," %%a in (%%x) do (
    echo %%b %%a 
    echo "-------------- Import of customer data - SCENARIO 3 /datacopy_pyhub import -m customer --code %%b --ddu %%a/ is STARTING"
    cmd /c datacopy_pyhub.cmd import -m customer --code %%b --ddu %%a
    if not errorlevel 1 (echo "-------------- Import of customer data - SCENARIO 3 /datacopy_pyhub import -m customer --code %%b --ddu %%a/ is COMPLETED with Exit Code=!ERRORLEVEL!") else (echo "ERROR -------------- Import of customer data - SCENARIO 3 /datacopy_pyhub import -m customer --code %%b --ddu %%a/ is FAILED with Exit Code=!ERRORLEVEL!" 
    EXIT /B !ERRORLEVEL! 
    ) 
  )
)

:export_mongo_db_collections_after_customer_import_scenario3
echo "---------- Export of mongo db collections after customer import Scenario 3 is starting"

rmdir /s /q %JSON_ETALON_DATA_DIR%\AfterImportScenario3

mongoexport --host %MongoDB__HOST_IP% --port %MongoDB__PORT% -u %MongoDB__USER% -p %MongoDB__PASSWORD% --authenticationDatabase=%MongoDB__AUTH_DATABASE% --authenticationMechanism=%MongoDB__AUTH_MECHANISM% --db %MongoDB__DATABASE% --collection foCounters --query "{customer: null}" --out %JSON_ETALON_DATA_DIR%\AfterImportScenario3\null\foCounters_null.json --jsonArray

for %%x in (foCounters foCounters.HSBC) do (
for %%y in (KITS AST BERMUDA INTEGRATION FAKITS FASMOKE CGA01 ) do (
mongoexport --host %MongoDB__HOST_IP% --port %MongoDB__PORT% -u %MongoDB__USER% -p %MongoDB__PASSWORD% --authenticationDatabase=%MongoDB__AUTH_DATABASE% --authenticationMechanism=%MongoDB__AUTH_MECHANISM% --db %MongoDB__DATABASE% --collection %%x --query "{customer: '%%y'}" --out %JSON_ETALON_DATA_DIR%\AfterImportScenario3\%%y\%%x_%%y.json --jsonArray
)
)
echo "---------- Export of mongo db collections after customer import Scenario 3 is completed"

:validation_of_customer_import_scenario3
echo "-------------- Validation of customer import Scenario 3 is STARTING"
rmdir /s /q %TEMP1%\
rmdir /s /q %TEMP2%\
mkdir %TEMP1%
mkdir %TEMP2%

xcopy /s /e /y /f %ETALON_AFTER_CUSTOMER_IMPORT_SCENARIO_3%\Tables %TEMP1%\Tables\

xcopy /s /e /y /f %DATA_PATH%\Tables %TEMP2%\Tables\


cd /D %TEMP1%
for /r %%d in (*) do copy "%%d" "%TEMP1%"

cd /D %TEMP2%
for /r %%d in (*) do copy "%%d" "%TEMP2%"

cd /D %TEMP1%

FOR %%i IN (*) DO (
echo %%i

echo n|comp "%TEMP1%\%%i" "%TEMP2%\%%i" >> d:\comparison_after_import_scenario3.txt
if not errorlevel 1 (echo "-------------- Comparison after customer import Scenario 3 passed for %%i with Exit Code=!ERRORLEVEL!") else (echo "ERROR -------------- Comparison after customer import Scenario 3 FAILED for %%i with Exit Code=!ERRORLEVEL!" 
EXIT /B !ERRORLEVEL! 
)
)

echo "-------------- Comparison of JSON after customer import Scenario 3 is STARTING" 

cd /D %TEST_HOME_DIR%

python %SCRIPTS_DIR%\Compare_JSON.py %JSON_ETALON_DATA_DIR%\AfterImport_etalon_scenario3\null\foCounters_null.json %JSON_ETALON_DATA_DIR%\AfterImportScenario3\null\foCounters_null.json
if not errorlevel 1 (echo "foCounters_null.json is OK") else (echo "ERROR -------------- Comparison after customer import Scenario 3 FAILED for foCounters_null.json with Exit Code=!ERRORLEVEL!" 
EXIT /B !ERRORLEVEL! 
) 
)

for %%x in (foCounters foCounters.HSBC) do (
for %%y in (AST BERMUDA INTEGRATION FAKITS FASMOKE CGA01) do (
echo n|comp "%JSON_ETALON_DATA_DIR%\beforeExport\%%y\%%x_%%y.json" "%JSON_ETALON_DATA_DIR%\AfterImportScenario3\%%y\%%x_%%y.json"
if not errorlevel 1 (echo "%%x_%%y.json is OK") else (echo "ERROR -------------- Comparison after customer import Scenario 3 FAILED for %%x_%%y.json with Exit Code=!ERRORLEVEL!" 
EXIT /B !ERRORLEVEL! 
) 
)
)
)

for %%x in (foCounters foCounters.HSBC) do (
for %%y in (KITS) do (
python %SCRIPTS_DIR%\Compare_JSON.py %JSON_ETALON_DATA_DIR%\empty\%%y\%%x_%%y.json %JSON_ETALON_DATA_DIR%\AfterImportScenario3\%%y\%%x_%%y.json
if not errorlevel 1 (echo "%%x_%%y.json is OK") else (echo "ERROR -------------- Comparison after customer import Scenario 3 FAILED for %%x_%%y.json with Exit Code=!ERRORLEVEL!" 
EXIT /B !ERRORLEVEL! 
) 
)
)
)

echo "-------------- Comparison of JSON after customer import Scenario 3 is COMPLETED" 

echo "-------------- Validation of customer import Scenario 3 is COMPLETED"




:clean_up_target_directories_and_mongo

rmdir /s /q %DATA_PATH%\keys
rmdir /s /q %DATA_PATH%\Tables
rmdir /s /q %DATA_PATH%\bkp_split
rmdir /s /q %DATA_PATH%\DB
rmdir /s /q %DATA_PATH%\Kanban\kStaIn

mkdir %DATA_PATH%\keys
mkdir %DATA_PATH%\Tables
mkdir %DATA_PATH%\bkp_split
mkdir %DATA_PATH%\DB
mkdir %DATA_PATH%\Kanban\kStaIn



mongo --host %MongoDB__HOST_IP% --port %MongoDB__PORT% -u %MongoDB__USER% -p %MongoDB__PASSWORD% --authenticationDatabase=%MongoDB__AUTH_DATABASE% --authenticationMechanism=%MongoDB__AUTH_MECHANISM% < %SCRIPTS_DIR%\mongo_drop_all.js

rmdir /s /q %EXPORT_DATA_DIR%\pyhub

mkdir %EXPORT_DATA_DIR%\pyhub\KITS
mkdir %EXPORT_DATA_DIR%\pyhub\SYSTEM

xcopy /E %SCENARIO_4_DATA%\KITS %EXPORT_DATA_DIR%\pyhub\KITS 
xcopy /E %SCENARIO_4_DATA%\SYSTEM %EXPORT_DATA_DIR%\pyhub\SYSTEM 

:import_system_data_dry_run
echo "-------------- Dry-run of /datacopy_pyhub import -m system/ is STARTING"
cd /D %BIN_DIRECTORY%
type %COMMON_DIR%\datacopy.properties

cmd /c datacopy_pyhub.cmd import -m system --dry-run
if not errorlevel 1 (echo "-------------- Dry-run of /datacopy_pyhub import -m system/ is COMPLETED with Exit Code=!ERRORLEVEL!") else (echo "ERROR -------------- Dry-run of /datacopy_pyhub import -m system/ is FAILED with Exit Code=!ERRORLEVEL!" 
EXIT /B !ERRORLEVEL! 
) 
)

:validation_of_import_system_data_dry_run
FOR /F "tokens=* USEBACKQ" %%S IN (`dir /b %DATA_PATH%\keys`) DO (
SET EMPTY=%%S
)

IF "%EMPTY%"=="" (echo "Folder %DATA_PATH%\keys is empty. Validation passed.") else (
echo "Folder %DATA_PATH%\keys is not empty. It contains unexpected files: "
dir /b %DATA_PATH%\keys
EXIT /b 2
)

FOR /F "tokens=* USEBACKQ" %%S IN (`dir /b %DATA_PATH%\Tables`) DO (
SET EMPTY=%%S
)

IF "%EMPTY%"=="" (echo "Folder %DATA_PATH%\Tables is empty. Validation passed.") else (
echo "Folder %DATA_PATH%\Tables is not empty. It contains unexpected files:"
dir /b %DATA_PATH%\Tables
EXIT /b 2
)

FOR /F "tokens=* USEBACKQ" %%S IN (`dir /b %DATA_PATH%\Kanban\kStaIn`) DO (
SET EMPTY=%%S
)

IF "%EMPTY%"=="" (echo "Folder %DATA_PATH%\Kanban\kStaIn is empty. Validation passed.") else (
echo "Folder %DATA_PATH%\Kanban\kStaIn is not empty. It contains unexpected files:"
dir /b %DATA_PATH%\Kanban\kStaIn
EXIT /b 2
)

FOR /F "tokens=* USEBACKQ" %%S IN (`dir /b %DATA_PATH%\bkp_split`) DO (
SET EMPTY=%%S
)

IF "%EMPTY%"=="" (echo "Folder %DATA_PATH%\bkp_split is empty. Validation passed.") else (
echo "Folder %DATA_PATH%\bkp_split is not empty. It contains unexpected files:"
dir /b %DATA_PATH%\bkp_split
)

FOR /F "tokens=* USEBACKQ" %%S IN (`dir /b %DATA_PATH%\bkp_split`) DO (
SET EMPTY=%%S
)

IF "%EMPTY%"=="" (echo "Folder %DATA_PATH%\DB is empty. Validation passed.") else (
echo "Folder %DATA_PATH%\DB is not empty. It contains unexpected files:"
dir /b %DATA_PATH%\DB
)


:import_system_data
echo "-------------- Import of system data /datacopy_pyhub import -m system/ is STARTING"
cd /D %BIN_DIRECTORY%
type %COMMON_DIR%\datacopy.properties

cmd /c datacopy_pyhub.cmd import -m system 
if not errorlevel 1 (echo "-------------- Import of system data /datacopy_pyhub import -m system/ is COMPLETED with Exit Code=!ERRORLEVEL!") else (echo "ERROR -------------- Import of system data /datacopy_pyhub import -m system/ is FAILED with Exit Code=!ERRORLEVEL!" 
EXIT /B !ERRORLEVEL! 
) 
)

:import_customer_data_scenario4

cd /D %BIN_DIRECTORY%

for %%x in (%CUSTOMER_ARRAY%) do (
  for /f "tokens=1-2 delims=," %%a in (%%x) do (
    echo %%b %%a 
    echo "-------------- Import of customer data - SCENARIO 4 /datacopy_pyhub import -m customer --code %%b --ddu %%a/ is STARTING"
    cmd /c datacopy_pyhub.cmd import -m customer --code %%b --ddu %%a
    if not errorlevel 1 (echo "-------------- Import of customer data - SCENARIO 4 /datacopy_pyhub import -m customer --code %%b --ddu %%a/ is COMPLETED with Exit Code=!ERRORLEVEL!") else (echo "ERROR -------------- Import of customer data - SCENARIO 4 /datacopy_pyhub import -m customer --code %%b --ddu %%a/ is FAILED with Exit Code=!ERRORLEVEL!" 
    EXIT /B !ERRORLEVEL! 
    ) 
  )
)

:export_mongo_db_collections_after_customer_import_scenario4
echo "---------- Export of mongo db collections after customer import Scenario 4 is starting"

rmdir /s /q %JSON_ETALON_DATA_DIR%\AfterImportScenario4

mongoexport --host %MongoDB__HOST_IP% --port %MongoDB__PORT% -u %MongoDB__USER% -p %MongoDB__PASSWORD% --authenticationDatabase=%MongoDB__AUTH_DATABASE% --authenticationMechanism=%MongoDB__AUTH_MECHANISM% --db %MongoDB__DATABASE% --collection foCounters --query "{customer: null}" --out %JSON_ETALON_DATA_DIR%\AfterImportScenario4\null\foCounters_null.json --jsonArray

for %%x in (foCounters foCounters.HSBC) do (
for %%y in (KITS AST BERMUDA INTEGRATION FAKITS FASMOKE CGA01 ) do (
mongoexport --host %MongoDB__HOST_IP% --port %MongoDB__PORT% -u %MongoDB__USER% -p %MongoDB__PASSWORD% --authenticationDatabase=%MongoDB__AUTH_DATABASE% --authenticationMechanism=%MongoDB__AUTH_MECHANISM% --db %MongoDB__DATABASE% --collection %%x --query "{customer: '%%y'}" --out %JSON_ETALON_DATA_DIR%\AfterImportScenario4\%%y\%%x_%%y.json --jsonArray
)
)
echo "---------- Export of mongo db collections after customer import Scenario 4 is completed"

:validation_of_customer_import_scenario4
echo "-------------- Validation of customer import Scenario 4 is STARTING"
rmdir /s /q %TEMP1%\
rmdir /s /q %TEMP2%\
mkdir %TEMP1%
mkdir %TEMP2%

xcopy /s /e /y /f %ETALON_AFTER_CUSTOMER_IMPORT_SCENARIO_4%\Tables %TEMP1%\Tables\
xcopy /s /e /y /f %DATA_PATH%\Tables %TEMP2%\Tables\


cd /D %TEMP1%
for /r %%d in (*) do copy "%%d" "%TEMP1%"

cd /D %TEMP2%
for /r %%d in (*) do copy "%%d" "%TEMP2%"

cd /D %TEMP1%

FOR %%i IN (*) DO (
echo %%i

echo n|comp "%TEMP1%\%%i" "%TEMP2%\%%i" >> d:\comparison_after_import_scenario4.txt
if not errorlevel 1 (echo "-------------- Comparison after customer import Scenario 4 passed for %%i with Exit Code=!ERRORLEVEL!") else (echo "ERROR -------------- Comparison after customer import Scenario 4 FAILED for %%i with Exit Code=!ERRORLEVEL!" 
EXIT /B !ERRORLEVEL! 
)
)

echo "-------------- Comparison of JSON after customer import Scenario 4 is STARTING" 

cd /D %TEST_HOME_DIR%

python %SCRIPTS_DIR%\Compare_JSON.py %JSON_ETALON_DATA_DIR%\AfterImport_etalon_scenario4\null\foCounters_null.json %JSON_ETALON_DATA_DIR%\AfterImportScenario4\null\foCounters_null.json
if not errorlevel 1 (echo "foCounters_null.json is OK") else (echo "ERROR -------------- Comparison after customer import Scenario 4 FAILED for foCounters_null.json with Exit Code=!ERRORLEVEL!" 
EXIT /B !ERRORLEVEL! 
) 
)

for %%x in (foCounters foCounters.HSBC) do (
for %%y in (AST BERMUDA INTEGRATION FAKITS FASMOKE CGA01) do (
python %SCRIPTS_DIR%\Compare_JSON.py %JSON_ETALON_DATA_DIR%\empty\%%y\%%x_%%y.json %JSON_ETALON_DATA_DIR%\AfterImportScenario4\%%y\%%x_%%y.json
if not errorlevel 1 (echo "%%x_%%y.json is OK") else (echo "ERROR -------------- Comparison after customer import Scenario 4 FAILED for %%x_%%y.json with Exit Code=!ERRORLEVEL!" 
EXIT /B !ERRORLEVEL! 
) 
)
)
)

for %%x in (foCounters foCounters.HSBC) do (
for %%y in (KITS) do (
python %SCRIPTS_DIR%\Compare_JSON.py %JSON_ETALON_DATA_DIR%\AfterImport_etalon_scenario4\%%y\%%x_%%y.json %JSON_ETALON_DATA_DIR%\AfterImportScenario4\%%y\%%x_%%y.json
if not errorlevel 1 (echo "%%x_%%y.json is OK") else (echo "ERROR -------------- Comparison after customer import Scenario 4 FAILED for %%x_%%y.json with Exit Code=!ERRORLEVEL!" 
EXIT /B !ERRORLEVEL! 
) 
)
)
)

echo "-------------- Comparison of JSON after customer import Scenario 4 is COMPLETED" 

echo "-------------- Validation of customer import Scenario 4 is COMPLETED"


:restore_customer_codes_in_oracle_db
echo "-------------- Restoring customer codes in Oracle DB is STARTING"
sqlplus %DB_CONNECTION_STRING% @%SCRIPTS_DIR%\restore_customer_codes_in_oracle_db.sql
echo "-------------- Changing customer codes in Oracle DB is COMPLETED"

:enable_constraints_in_oracle_db
echo "-------------- Enabling constraints in Oracle DB is STARTING"
sqlplus %DB_CONNECTION_STRING% @%SCRIPTS_DIR%\enable_constraints.sql
echo "-------------- Enabling constraints in Oracle DB is COMPLETED"

EXIT /B %ERRORLEVEL%
