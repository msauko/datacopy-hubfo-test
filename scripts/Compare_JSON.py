#!/usr/bin/python

import sys, getopt, json
from collections import OrderedDict


    
def main(argv):
    file1 = argv[0]
    file2 = argv[1]
 
    f2 = open(file2, 'r')
    d2 = f2.read()
    js2 = json.loads(d2)
    js2.sort(key=lambda x: x['counter'])
 
    a = json.dumps(js2, sort_keys=True)
    
    f1 = open(file1, 'r')
    d1 = f1.read()
    js1 = json.loads(d1)
    js1.sort(key=lambda x: x['counter'])

    b = json.dumps(js1, sort_keys=True)
    
    json_file1 = json.loads(a)
    for i in json_file1:
        i["_id"] = None
    
    json_file2 = json.loads(b)
    for i in json_file2:
        i["_id"] = None
        
    
    print json_file1 == json_file2
    sys.exit(0) if json_file1 == json_file2 else sys.exit(1)
    
    
if __name__ == "__main__":
   main(sys.argv[1:])