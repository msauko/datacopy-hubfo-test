set serveroutput on

declare
        sql_text varchar2(1000);
begin
        for c in (
                select 'ALTER TABLE '||owner||'.'||table_name||' enable constraint '||constraint_name as sqltext
                from all_constraints where r_constraint_name in (select constraint_name from all_constraints where table_name='CORPORATE_CUSTOMER')
                )
        loop
                sql_text := c.sqltext;
                sys.dbms_output.put_line('Query: ' || sql_text);
                execute immediate sql_text;
                sys.dbms_output.put_line('Query executed.');
        end loop;
end;
/

exit