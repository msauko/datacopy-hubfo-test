from random import randrange

open_file_pay = open('foCounters/foCounters.HSBC.json', 'w')

customerName_list = ["KITS", "AST","INTEGRATION","FAKITS","FASMOKE","BERMUDA","CGA01"]
paymentType_list = ["SALA", "SUP", "SAL", "SALE", "BUY", "SA", "SALES"]
connectId_list = ["CONNECTID_01", "CONNECTID_02", "CONNECTID_03", "CONNECTID_04", "CONNECTID_05", "CONNECTID_06", "CONNECTID_07"]
country_list = ["United States", "France", "Germany", "Great Britain", "Russia", "Belarus", "Spain"]
company_list = ["Kyriba", "Fire Apps","Kyriba", "Fire Apps","Kyriba", "Fire Apps","Kyriba"]
counter_list = ['1', '22', '333', '4444', '55555', '666666', '7777777']
batchCodes_list = ["LA01", "LA02","LA03", "LA04", "SA01","SA02","SA03"]
netId_list = ["NETID_01", "NETID_02", "NETID_03","NETID_04", "NETID_05", "NETID_06", "NETID_07"]
str_list = ["fde", "dfd", "ger","cvd", "dfe", "gfd", "ytw"]

template = """
{
    "_id": ObjectId("5cfa7ae42c5c%(RANDOM)dfd64ad1"),
    "customer" : "%(CUSTOMER)s",
    "payment_type" : "%(PAYMENT_TYPE)s",
    "connect_id" : "%(CONNECT_ID)s",
    "country" : "%(COUNTRY)s",
    "company" : "%(COMPANY)s",
    "counter" : %(COUNTER)s,
    "batch_codes" : [
        "%(BATCH_CODES)s"
    ],
   "net_id" : "%(NET_ID)s"
}\n\n
"""

with open("foCounters/foCounters.HSBC.json", "w") as thePYFile:
    for i in xrange(0, len(customerName_list)):
        for z in xrange(0, len(counter_list)):
                random_integer = randrange(11111, 99989)
                open_file_pay.write(template % {'CUSTOMER': customerName_list[i], 'PAYMENT_TYPE': paymentType_list[i], 'CONNECT_ID': connectId_list[i],
                                                'COUNTRY': country_list[i], 'COMPANY': company_list[i], 'RANDOM_STR': str_list[i],
                                                'COUNTER': counter_list[z],
                                                'BATCH_CODES': batchCodes_list[i], 'NET_ID': netId_list[i], 'RANDOM': random_integer})

open_file_pay.close()