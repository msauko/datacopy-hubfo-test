from datetime import datetime, timedelta
from random import randrange

open_file_pay = open('foCounters/foCounters.json', 'w')


bankCode_list = ['BANK1', 'BANK2', 'BANK3', 'BANK4','BANK5','BANK6','BANK7','BANK8','BANK9','BANK10','BANK11','BANK12']
customerName_list = ["KITS", "AST","INTEGRATION","FAKITS","FASMOKE","BERMUDA","CGA01"]

#Format list in foCounters collection when customers always = NOT NULL
formatName_list = ["BTL91", "CBI_XML_DD","MT202","NACHA","PMXML","SEPADD","XMLPAIN","CIBC_TLV"]

#Format list in foCounters collection when customers always = NULL
formatName_list_2 = ["BACSFF", "BR24PB","BWIFF","CBI_XML_CT","DE_SEPACT","EDI820_HUNTINGTON","EDI820_RBC","EDI820_TD","MT103","SEPACT","CIBC_TLV"]

#Format list in foCounters collection when customers may be NULL or NOT NULL
formatName_list_3 = ["CIBC_TLV"]

counter_list = ['1', '22', '333', '4444', '55555', '666666', '7777777', '88888888', '999999999','49','115','227']
date_list = ["2019-09-22T00:00:00.000Z", "2019-09-21T00:00:00.000Z", "2019-09-20T00:00:00.000Z", "2019-09-19T00:00:00.000Z", "2019-09-18T00:00:00.000Z", "2019-09-17T00:00:00.000Z", "2019-09-16T00:00:00.000Z", "2019-09-15T00:00:00.000Z", "2019-09-14T00:00:00.000Z", "2019-09-13T00:00:00.000Z", "2019-09-12T00:00:00.000Z"]



template = """
{
   "_id": ObjectId("5cfa7ae42c5c%(RANDOM)dfd64ad1"),
   "customer" : "%(CUSTOMER)s",
   "form_id" : null,
   "reset_date" : ISODate("%(DATETIME)s"),
   "format" : "%(FORMAT)s",
   "counter" : %(COUNTER)s,
   "payor_ac_bank_code" : "%(BANK)s",
   "date" : ISODate("%(DATETIME)s")
}\n\n
"""

with open("foCounters/foCounters.json", "w") as thePYFile:
    for i in xrange(0, len(formatName_list)):
        for y in xrange(0, len(customerName_list)):
            random_integer = randrange(11111, 99989)
            open_file_pay.write(template % {'FORMAT': formatName_list[i], 'CUSTOMER': customerName_list[y],
                                        'COUNTER': counter_list[i],
                                        'BANK': bankCode_list[i], 'DATETIME': date_list[i], 'RANDOM': random_integer})




template_2 = """
{
   "_id": ObjectId("5cfa7ae42c5c%(RANDOM)dfd64ad1"),
   "customer" : null,
   "form_id" : null,
   "reset_date" : ISODate("%(DATETIME)s"),
   "format" : "%(FORMAT)s",
   "counter" : %(COUNTER)s,
   "payor_ac_bank_code" : "%(BANK)s",
   "date" : ISODate("%(DATETIME)s")
}\n\n
"""

with open("foCounters/foCounters.json", "a+") as thePYFile_2:
    for i in xrange(0, len(formatName_list_2)):
            random_integer = randrange(11111, 99989)
            open_file_pay.write(template_2 % {'FORMAT': formatName_list_2[i],
                                        'COUNTER': counter_list[i],
                                        'BANK': bankCode_list[i], 'DATETIME': date_list[i], 'RANDOM': random_integer})
                                        
                                        

open_file_pay.close()